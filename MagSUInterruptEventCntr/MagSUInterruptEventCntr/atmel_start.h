#ifndef ATMEL_START_H_INCLUDED
#define ATMEL_START_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

#include "driver_init.h"
#include "stdio_start.h"

/**
 * Initializes MCU, drivers and middleware in the project
 **/
void atmel_start_init(void);



#define MAX_WAKEUP_INTERVAL 1439
#define MIN_WAKEUP_INTERVAL 0
#define MIN_SAMPLING_RATE 0				// 12.5 Hz
#define MAX_SAMPLING_RATE 11			// 6664 Hz
#define MIN_FULL_SCALE_RANGE 0			// 2g
#define MAX_FULL_SCALE_RANGE 3			// 0 - 2g, 1 - 16g, 2 - 4g, 3 - 8g
#define MIN_DATA_SET 1
#define MAX_DATA_SET 65535
#define MAX_AP_TIMEOUT 43200
#define MAX_GW_TIMEOUT 43200
#define MIN_AP_TIMEOUT 2
#define MIN_GW_TIMEOUT 2
#define MAX_TEMP_INTERVAL 250
#define MIN_TEMP_INTERVAL 1

#ifdef __cplusplus
}
#endif
#endif
