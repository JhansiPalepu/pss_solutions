/*
 * eeprom.h
 *
 * Created: 6/16/2017 9:18:23 AM
 *  Author: Ponmani
 */ 


#ifndef EEPROM_H_
#define EEPROM_H_

typedef struct
{
	uint8_t samplingRate;
	uint8_t fullScaleRange;
	uint8_t intervalInMinLow;		// Maximum of 45 days -  Not used in MMUE
	uint8_t intervalInMinHigh;		// Not used in MMUE
	uint8_t bwCtrl1Reg;
	uint8_t bwCtrl8Reg;
	uint8_t dataSetLow;				// Total data set to be transferred
	uint8_t dataSetHigh;
}userSettingsStruct;




#endif /* EEPROM_H_ */