/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file or main.c
 * to avoid loosing it when reconfiguring.
 */

#include "atmel_start.h"
#include "string.h"
#include "winc_init.h"
#include "driver/source/nmasic.h"

extern void set_winc_spi_descriptor(struct spi_m_sync_descriptor *spi_inst);


