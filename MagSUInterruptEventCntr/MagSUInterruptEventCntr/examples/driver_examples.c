/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */

#include "driver_examples.h"
#include "driver_init.h"
#include "utils.h"

static void button_on_PA02_pressed(void)
{
}

static void button_on_PB09_pressed(void)
{
}

/**
 * Example of using EXTIRQ_INSTANCE
 */
void EXTIRQ_INSTANCE_example(void)
{

	ext_irq_register(PIN_PA02, button_on_PA02_pressed);
	ext_irq_register(PIN_PB09, button_on_PB09_pressed);
}

static uint8_t src_data[128];
static uint8_t chk_data[128];
/**
 * Example of using FLASH_0 to read and write Flash main array.
 */
void FLASH_0_example(void)
{
	uint32_t page_size;
	uint16_t i;

	/* Init source data */
	page_size = flash_get_page_size(&FLASH_0);

	for (i = 0; i < page_size; i++) {
		src_data[i] = i;
	}

	/* Write data to flash */
	flash_write(&FLASH_0, 0x3200, src_data, page_size);

	/* Read data from flash */
	flash_read(&FLASH_0, 0x3200, chk_data, page_size);
}

/**
 * Example of using SPI_INSTANCE to write "Hello World" using the IO abstraction.
 */
static uint8_t example_SPI_INSTANCE[12] = "Hello World!";

void SPI_INSTANCE_example(void)
{
	struct io_descriptor *io;
	spi_m_sync_get_io_descriptor(&SPI_INSTANCE, &io);

	spi_m_sync_enable(&SPI_INSTANCE);
	io_write(io, example_SPI_INSTANCE, 12);
}

/**
 * Example of using Flash_SPI to write "Hello World" using the IO abstraction.
 */
static uint8_t example_Flash_SPI[12] = "Hello World!";

void Flash_SPI_example(void)
{
	struct io_descriptor *io;
	spi_m_sync_get_io_descriptor(&Flash_SPI, &io);

	spi_m_sync_enable(&Flash_SPI);
	io_write(io, example_Flash_SPI, 12);
}

/**
 * Example of using TARGET_IO to write "Hello World" using the IO abstraction.
 */
void TARGET_IO_example(void)
{
	struct io_descriptor *io;
	usart_sync_get_io_descriptor(&TARGET_IO, &io);
	usart_sync_enable(&TARGET_IO);

	io_write(io, (uint8_t *)"Hello World!", 12);
}

void delay_example(void)
{
	delay_ms(5000);
}

/**
 * Example of using CALENDAR_0.
 */
static struct calendar_alarm alarm;

static void alarm_cb(struct calendar_descriptor *const descr)
{
	/* alarm expired */
}

void CALENDAR_0_example(void)
{
	struct calendar_date date;
	struct calendar_time time;

	calendar_enable(&CALENDAR_0);

	date.year  = 2000;
	date.month = 12;
	date.day   = 31;

	time.hour = 12;
	time.min  = 59;
	time.sec  = 59;

	calendar_set_date(&CALENDAR_0, &date);
	calendar_set_time(&CALENDAR_0, &time);

	alarm.cal_alarm.datetime.time.sec = 4;
	alarm.cal_alarm.option            = CALENDAR_ALARM_MATCH_SEC;
	alarm.cal_alarm.mode              = REPEAT;

	calendar_set_alarm(&CALENDAR_0, &alarm, alarm_cb);
}
