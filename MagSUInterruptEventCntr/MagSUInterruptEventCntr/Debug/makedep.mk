################################################################################
# Automatically-generated file. Do not edit or delete the file
################################################################################

atmel_start.c

Device_Startup\startup_samd21.c

Device_Startup\system_samd21.c

driver_init.c

examples\driver_examples.c

hal\src\hal_atomic.c

hal\src\hal_calendar.c

hal\src\hal_delay.c

hal\src\hal_evsys.c

hal\src\hal_ext_irq.c

hal\src\hal_flash.c

hal\src\hal_gpio.c

hal\src\hal_init.c

hal\src\hal_io.c

hal\src\hal_sleep.c

hal\src\hal_spi_m_sync.c

hal\src\hal_usart_sync.c

hal\utils\src\utils_assert.c

hal\utils\src\utils_event.c

hal\utils\src\utils_list.c

hal\utils\src\utils_syscalls.c

hpl\core\hpl_core_m0plus_base.c

hpl\core\hpl_init.c

hpl\dmac\hpl_dmac.c

hpl\eic\hpl_eic.c

hpl\evsys\hpl_evsys.c

hpl\gclk\hpl_gclk.c

hpl\nvmctrl\hpl_nvmctrl.c

hpl\pm\hpl_pm.c

hpl\rtc\hpl_rtc.c

hpl\sercom\hpl_sercom.c

hpl\sysctrl\hpl_sysctrl.c

hpl\systick\hpl_systick.c

hpl\tc\tc_lite.c

Internal_Flash.c

main.c

SPI_Flash.c

stdio_redirect\gcc\read.c

stdio_redirect\gcc\write.c

stdio_redirect\stdio_io.c

stdio_start.c

winc1500\bsp\source\nm_bsp.c

winc1500\bus_wrapper\source\nm_bus_wrapper.c

winc1500\common\source\nm_common.c

winc1500\driver\source\m2m_ate_mode.c

winc1500\driver\source\m2m_crypto.c

winc1500\driver\source\m2m_hif.c

winc1500\driver\source\m2m_ota.c

winc1500\driver\source\m2m_periph.c

winc1500\driver\source\m2m_ssl.c

winc1500\driver\source\m2m_wifi.c

winc1500\driver\source\nmasic.c

winc1500\driver\source\nmbus.c

winc1500\driver\source\nmdrv.c

winc1500\driver\source\nmi2c.c

winc1500\driver\source\nmspi.c

winc1500\driver\source\nmuart.c

winc1500\socket\source\socket.c

winc1500\spi_flash\source\spi_flash.c

winc_init.c

