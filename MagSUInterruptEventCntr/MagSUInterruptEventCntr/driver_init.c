/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */

#include "driver_init.h"
#include <peripheral_clk_config.h>
#include <utils.h>
#include <hal_init.h>
#include <hpl_gclk_base.h>
#include <hpl_pm_base.h>
#include "tc_lite.h"

struct spi_m_sync_descriptor SPI_INSTANCE;
struct spi_m_sync_descriptor Flash_SPI;

struct flash_descriptor FLASH_0;

struct usart_sync_descriptor TARGET_IO;

struct calendar_descriptor CALENDAR_0;

void EXTIRQ_INSTANCE_init(void)
{
	_gclk_enable_channel(EIC_GCLK_ID, CONF_GCLK_EIC_SRC);

	// Set pin direction to input
	gpio_set_pin_direction(SWITCH, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(SWITCH,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_OFF);

	gpio_set_pin_function(SWITCH, PINMUX_PA02A_EIC_EXTINT2);

	// Set pin direction to input
	gpio_set_pin_direction(CONF_WINC_EXT_INT_PIN, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(CONF_WINC_EXT_INT_PIN,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_OFF);

	gpio_set_pin_function(CONF_WINC_EXT_INT_PIN, PINMUX_PB09A_EIC_EXTINT9);

	ext_irq_init();
}

void FLASH_0_CLOCK_init(void)
{

	_pm_enable_bus_clock(PM_BUS_APBB, NVMCTRL);
}

void FLASH_0_init(void)
{
	FLASH_0_CLOCK_init();
	flash_init(&FLASH_0, NVMCTRL);
}

void SPI_INSTANCE_PORT_init(void)
{

	gpio_set_pin_level(PA12,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(PA12, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(PA12, PINMUX_PA12C_SERCOM2_PAD0);

	gpio_set_pin_level(PA13,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(PA13, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(PA13, PINMUX_PA13C_SERCOM2_PAD1);

	// Set pin direction to input
	gpio_set_pin_direction(PA15, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(PA15,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_OFF);

	gpio_set_pin_function(PA15, PINMUX_PA15C_SERCOM2_PAD3);
}

void SPI_INSTANCE_CLOCK_init(void)
{
	_pm_enable_bus_clock(PM_BUS_APBC, SERCOM2);
	_gclk_enable_channel(SERCOM2_GCLK_ID_CORE, CONF_GCLK_SERCOM2_CORE_SRC);
}

void SPI_INSTANCE_init(void)
{
	SPI_INSTANCE_CLOCK_init();
	spi_m_sync_init(&SPI_INSTANCE, SERCOM2);
	SPI_INSTANCE_PORT_init();
}

void Flash_SPI_PORT_init(void)
{

	gpio_set_pin_level(FLASH_MOSI,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(FLASH_MOSI, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(FLASH_MOSI, PINMUX_PA22C_SERCOM3_PAD0);

	gpio_set_pin_level(FLASH_SCK,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(FLASH_SCK, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(FLASH_SCK, PINMUX_PA23C_SERCOM3_PAD1);

	// Set pin direction to input
	gpio_set_pin_direction(FLASH_MISO, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(FLASH_MISO,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_OFF);

	gpio_set_pin_function(FLASH_MISO, PINMUX_PA21D_SERCOM3_PAD3);
}

void Flash_SPI_CLOCK_init(void)
{
	_pm_enable_bus_clock(PM_BUS_APBC, SERCOM3);
	_gclk_enable_channel(SERCOM3_GCLK_ID_CORE, CONF_GCLK_SERCOM3_CORE_SRC);
}

void Flash_SPI_init(void)
{
	Flash_SPI_CLOCK_init();
	spi_m_sync_init(&Flash_SPI, SERCOM3);
	Flash_SPI_PORT_init();
}

void TARGET_IO_PORT_init(void)
{

	gpio_set_pin_function(TXD, PINMUX_PB10D_SERCOM4_PAD2);

	gpio_set_pin_function(RXD, PINMUX_PB11D_SERCOM4_PAD3);
}

void TARGET_IO_CLOCK_init(void)
{
	_pm_enable_bus_clock(PM_BUS_APBC, SERCOM4);
	_gclk_enable_channel(SERCOM4_GCLK_ID_CORE, CONF_GCLK_SERCOM4_CORE_SRC);
}

void TARGET_IO_init(void)
{
	TARGET_IO_CLOCK_init();
	usart_sync_init(&TARGET_IO, SERCOM4, (void *)NULL);
	TARGET_IO_PORT_init();
}

void delay_driver_init(void)
{
	delay_init(SysTick);
}

void CALENDAR_0_CLOCK_init(void)
{
	_pm_enable_bus_clock(PM_BUS_APBA, RTC);
	_gclk_enable_channel(RTC_GCLK_ID, CONF_GCLK_RTC_SRC);
}

void CALENDAR_0_init(void)
{
	CALENDAR_0_CLOCK_init();
	calendar_init(&CALENDAR_0, RTC);
}

void TIMER_0_CLOCK_init(void)
{
	_pm_enable_bus_clock(PM_BUS_APBC, TC4);
	_gclk_enable_channel(TC4_GCLK_ID, CONF_GCLK_TC4_SRC);
}

void EVENT_SYSTEM_0_init(void)
{
	_gclk_enable_channel(EVSYS_GCLK_ID_0, CONF_GCLK_EVSYS_CHANNEL_0_SRC);

	_pm_enable_bus_clock(PM_BUS_APBC, EVSYS);

	event_system_init();
}



/*
* For power optimization and to reduce leakage current when the master is powered up and the slave is powered off
* The pins used for master - slave communication needs to be parked in known state in order to reduce the leakage current
* PA12	INPUT	PULL UP
* PA13	INPUT	PULL UP
* PA15	INPUT	PULL UP
*/

void SPI_INSTANCE_PORT_uninit(void)
{
	// <y> Pull configuration - <GPIO_PULL_OFF"> Off,  <GPIO_PULL_UP"> Pull-up, <GPIO_PULL_DOWN"> Pull-down
	gpio_set_pin_direction(PA12, GPIO_DIRECTION_IN);
	gpio_set_pin_pull_mode(PA12, GPIO_PULL_OFF);
	gpio_set_pin_function(PA12, GPIO_PIN_FUNCTION_OFF);
	
	gpio_set_pin_direction(PA13, GPIO_DIRECTION_IN);
	gpio_set_pin_pull_mode(PA13, GPIO_PULL_OFF);
	gpio_set_pin_function(PA13, GPIO_PIN_FUNCTION_OFF);

	gpio_set_pin_direction(PA15, GPIO_DIRECTION_IN);
	gpio_set_pin_pull_mode(PA15, GPIO_PULL_OFF);
	gpio_set_pin_function(PA15, GPIO_PIN_FUNCTION_OFF);
}



void Flash_SPI_PORT_deinit(void)
{	// Pull configuration
	// <GPIO_PULL_OFF"> Off
	// <GPIO_PULL_UP"> Pull-up
	// <GPIO_PULL_DOWN"> Pull-down

	// Set pin direction to input
	gpio_set_pin_direction(FLASH_MISO, GPIO_DIRECTION_OUT);
	gpio_set_pin_level(FLASH_MISO,	false);
	gpio_set_pin_function(FLASH_MISO, GPIO_PIN_FUNCTION_OFF);

	// Set pin direction to input
	gpio_set_pin_direction(FLASH_MOSI, GPIO_DIRECTION_OUT);
	gpio_set_pin_level(FLASH_MOSI,	false);
	gpio_set_pin_function(FLASH_MOSI, GPIO_PIN_FUNCTION_OFF);

	// Set pin direction to input
	gpio_set_pin_direction(FLASH_SCK, GPIO_DIRECTION_OUT);
	gpio_set_pin_level(FLASH_SCK, false);
	gpio_set_pin_function(FLASH_SCK, GPIO_PIN_FUNCTION_OFF);
	
	
}
void system_init(void)
{
	init_mcu();

	// GPIO on PA04

	// Set pin direction to input
	gpio_set_pin_direction(SPR1, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(SPR1,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_OFF);

	gpio_set_pin_function(SPR1, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA06

	gpio_set_pin_level(SENSOR_REG_EN,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(SENSOR_REG_EN, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(SENSOR_REG_EN, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA07

	gpio_set_pin_level(FLASH_CS,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   true);

	// Set pin direction to output
	gpio_set_pin_direction(FLASH_CS, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(FLASH_CS, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA10

	// Set pin direction to input
	gpio_set_pin_direction(DBG, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(DBG,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_OFF);

	gpio_set_pin_function(DBG, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA11

	gpio_set_pin_level(PWRONIND,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(PWRONIND, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(PWRONIND, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA14

	gpio_set_pin_level(CS_PIN,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   true);

	// Set pin direction to output
	gpio_set_pin_direction(CS_PIN, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(CS_PIN, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA17

	gpio_set_pin_level(SLAVE_SELECT,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   true);

	// Set pin direction to output
	gpio_set_pin_direction(SLAVE_SELECT, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(SLAVE_SELECT, GPIO_PIN_FUNCTION_OFF);


	// GPIO on PA27

	gpio_set_pin_level(RESET_PIN,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(RESET_PIN, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(RESET_PIN, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA28

	gpio_set_pin_level(CE_PIN,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(CE_PIN, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(CE_PIN, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PB22

	gpio_set_pin_level(MEASEN,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(MEASEN, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(MEASEN, GPIO_PIN_FUNCTION_OFF);


	EXTIRQ_INSTANCE_init();

	FLASH_0_init();

	SPI_INSTANCE_init();

	Flash_SPI_init();

	TARGET_IO_init();

	delay_driver_init();

	CALENDAR_0_init();

	TIMER_0_CLOCK_init();

	TIMER_0_init();

	EVENT_SYSTEM_0_init();
}
