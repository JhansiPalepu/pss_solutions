/*
* SPI_Flash.h
*
* Created: 24/8/2018 
*  Author: Jhansi Palepu
*/

#include <stdbool.h>
#include "winc1500/driver/include/m2m_types.h"


#define SSID_MIN_LENGTH 4			// Including "\0" character
#define SSID_MAX_LENGTH 16			// Including "\0" character
#define PWD_MIN_LENGTH 9			// Including "\0" character
#define PWD_MAX_LENGTH 16			// Including "\0" character
#define CONFIG_DATA_LENGTH 8

#define  FLASH_ACCESS_ERROR 0xFFFFFFFF



#define MAX_SSID_LENGTH 16 // Including null character
#define MIN_SSID_LENGTH 4 // Including null character

#define MAX_PWD_LENGTH 16 // Including null character
#define MIN_PWD_LENGTH 9 // Including null character

#define FLASH_ACCESS_SUCCESS  0


//  size of application section is 112KB and page size is 64 bytes; hence NO_OF_PAGES_TO_ERASE is (112*1024)/64 = 1792
// for safety we reducing one page to erase so that it will not erase other portion
#define  NO_OF_PAGES_TO_ERASE 1791

#define TIME_INFO_ADDRESS 0x1FA000
//#define PREVIOUSLY_ERASED_BLOCK_ADDRESS 0x1FF0DE


#define APP_SECTION2_ADDRESS 0x00020000
#define APP_SECTION1_ADDRESS 0x0004000
#define EEPROM_ADDRESS 0x0003D000
#define DEV_APP_VER_ADD 0x0003F000
#define  WIFI_APP_VER_ADD 0x0003E000

#define SSID_ADDRESS  0x0003C100
#define PASSWORD_ADDRESS 0x0003C200
#define GWIP_ADDRESS 0x0003C300
#define TIMEOUT_ADDRESS 0x0003C400


//Function declarations

bool writePWDIntoInternalFlash(uint8_t* pwdData, uint8_t pwdLength);
bool writeSSIDIntoInternalFlash(uint8_t* ssidData, uint8_t ssidLength);
bool writeGWIPIntoInternalFLASH(uint32_t gatewaysIP);
bool readGWIPFromInternalFLASH(void);
bool writeTimeoutsIntoInternalFLASH(uint16_t APtimeout, uint16_t GWtimeout);
bool readtimeoutsFromInternalFLASH(uint16_t *APtimeOut, uint16_t *GWtimeOut);
bool checkSSID(uint8_t* ssidData, uint8_t ssidLength);
bool checkPWD(uint8_t* pwdData, uint8_t pwdLength);
bool ssidPwdAvailable(uint8_t *ssidData, uint8_t* pwdData);
bool GWIPValid(uint32_t *gwIP);


