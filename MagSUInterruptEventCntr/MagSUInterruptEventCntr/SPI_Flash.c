/*
* SPI_Flash.c
*
* Created: 8/3/2017 11:30:42 AM
*  Author: Ramachandra
*/
#include <atmel_start.h>
#include "SPI_Flash.h"
#include <stdbool.h>

bool g_hwDebugMode;


uint32_t flashOverflowAddress = MAX_USABLE_FLASH_ADDRESS; 

/* Checks if the flash is on board and if present,
reads its manufacturer ID and device ID and make sure
that matches with the correct one. This is check is
done as this does not have WHO AM I register */
bool isFlashReady(struct io_descriptor *in_out)
{
	bool result = false;
	uint8_t ID_array[3] = {0};
	
//	pwr_dwn_release(in_out);
	if(read_manufacturer_device_ID(in_out, ID_array))
	{
		printf("flash manf id :%s",ID_array);
		if(ID_array[0] != FLASH_MANUFACTURER_ID)
		{
			if(g_hwDebugMode)
			{
				printf("FLASH Identification ERROR: Manufacturer ID does not match!\n");
				printf("FLASH: Manufacturer ID - %d\n", ID_array[0]);
			}
		}
		
		if(ID_array[1] != FLASH_DEVICE_ID)
		{
			if(g_hwDebugMode)
			{
				printf("FLASH Identification ERROR: Device ID does not match!\n");
				printf("FLASH: DevID - %d\n", ID_array[1]);
			}
		}
		
		if (ID_array[0] == FLASH_MANUFACTURER_ID && ID_array[1] == FLASH_DEVICE_ID)
		{
			result = true;
			// For the first time when the Flash memory is used, all locations will have '0xff', so it is made zero.
			// The protocol followed is in a way that "NEXT_DATA_TO_BE_SENT" is 0 while we write into flash for the very first time
			uint8_t readData[2] ={0};
			/*read_data(in_out, NEXT_DATA_TO_BE_SENT, 1, readData);				//! @todo do we need to check the return value of read_data?
			// If value in NEXT_DATA_TO_BE_SENT is 0xFF, then make it 0
			if(readData[0] == 0xFF)
			{
				readData[0] = 0;
				// Only the page programming is possible. Not the individual location write;
				write_ID_settings_data(in_out, NEXT_DATA_TO_BE_SENT, readData, 1);
			}

			readData[0] = 0;
			read_data(in_out, NUMBER_OF_BLOCKS_WRITTEN_ADDRESS, 1, readData);
			// If number of block written is 0xFF, then make it 0
			if(readData[0] == 0xFF)
			{
				readData[0] = 0;
				write_ID_settings_data(in_out, NUMBER_OF_BLOCKS_WRITTEN_ADDRESS, readData, 1);
			}*/
			// Added for flash self test
			readData[0] = 0;
			read_data(in_out, CORRUPTED_FLASH_BLOCKS_ADDRESS, 1, readData);
			// If number of block written is 0xFF, then make it 0
			if(readData[0] == 0xFF)
			{
				readData[0] = 0;
				write_ID_settings_data(in_out, CORRUPTED_FLASH_BLOCKS_ADDRESS, readData, 1);
			}
			if(g_hwDebugMode)
			{
				printf("Number of corrupted flash blocks:%d\n", readData[0]);
				printf("Number of usable FLASH blocks:%d\n\n", (63-readData[0]));
			}
		}
		
	}
	else
	{
		if(g_hwDebugMode)
		printf("else of read_manufacturer_device_ID(in_out, ID_array)\n");
	}
	
	return result;
}

/**Sets the WEL bit in the Status register1*/

void write_enable(struct io_descriptor *in_out)
{
	uint8_t data_array[2] = {WRITE_ENABLE,0};
	gpio_set_pin_level(FLASH_CS, false); //Pull chip select low
	io_write(in_out, (uint8_t*)data_array, 1); //Send the instruction on to the DI line
	gpio_set_pin_level(FLASH_CS, true);// Pull chip select high
	//	delay_ms(1);
}

/**Disables the WEL bit in the Status register1*/

void write_disable(struct io_descriptor *in_out)
{
	uint8_t data_array[2] = {WRITE_DISABLE,0};
	gpio_set_pin_level(FLASH_CS, false); //Pull chip select low
	io_write(in_out, (uint8_t*)data_array, 1); //Send the instruction on to the DI line
	gpio_set_pin_level(FLASH_CS, true);// Pull chip select high
	delay_ms(1);
}

/**Enables us to change the volatile bits in the status registers.
*/

void write_enable_volatile_reg(struct io_descriptor *in_out)
{
	uint8_t data_array[2] = {WR_EN_VOL_SR,0};
	gpio_set_pin_level(FLASH_CS, false); //Pull chip select low
	io_write(in_out, (uint8_t*)data_array, 1); //Send the instruction on to the DI line
	gpio_set_pin_level(FLASH_CS, true);// Pull chip select high
	delay_ms(1);
}
/*
* Reads the contents of status register 1
* @return true if the read is success, false otherwise
*/
bool read_status_reg1(struct io_descriptor *in_out, uint8_t *regValue)
{
	uint8_t result = false;
	uint8_t data_array[2] = {READ_STATUS_REG1,0};
	gpio_set_pin_level(FLASH_CS, false);						// Pull chip select low
	if(io_write(in_out, (uint8_t*)data_array, 1) == 1)			// Send the instruction on to the DI line
	{
		if(io_read(in_out, (uint8_t*)&data_array[1], 1) == 1)	// Read the data from the DO line
		{
			result = true;
		}
	}
	gpio_set_pin_level(FLASH_CS, true);							// Pull chip select high
	*regValue = data_array[1];
	return result;
}

/**Reads the contents of status register 2
*/
uint8_t read_status_reg2(struct io_descriptor *in_out)
{
	uint8_t result;
	uint8_t data_array[2] = {READ_STATUS_REG2,0};
	gpio_set_pin_level(FLASH_CS, false);//Pull chip select low
	io_write(in_out, (uint8_t*)data_array, 1); //Send the instruction on to the DI line
	io_read(in_out, (uint8_t*)&data_array[1], 1);// Read the data from the DO line
	gpio_set_pin_level(FLASH_CS, true);// Pull chip select high
	result = data_array[1];
	return result;
}

/**Reads the contents of status register 3
*/
uint8_t read_status_reg3(struct io_descriptor *in_out)
{
	uint8_t result;
	uint8_t data_array[2] = {READ_STATUS_REG3,0};
	gpio_set_pin_level(FLASH_CS, false);//Pull chip select low
	io_write(in_out, (uint8_t*)data_array, 1); //Send the instruction on to the DI line
	io_read(in_out, (uint8_t*)&data_array[1], 1);// Read the data from the DO line
	gpio_set_pin_level(FLASH_CS, true);// Pull chip select high
	result = data_array[1];
	return result;
}
/**Checks the BUSY bit the status register1
* @return Returns true if the BUSY bit is HIGH(FLASH is busy), else returns false
*/
bool check_BUSY_bit(struct io_descriptor *in_out)
{
	uint8_t readData = 0;
	bool result = false;
	// If the status register successfully read, check if the busy bit is set
	if(read_status_reg1(in_out, &readData))
	{
		readData = (readData & (1<<BUSY_BIT));
		if(readData == 0x01)
			result = true;
	}
	return result;
}
/**Checks the SUS bit the status register2
* @return Returns true if the SUSPEND bit is HIGH, else returns false
*/
bool check_SUS_bit(struct io_descriptor *in_out)
{
	uint8_t readData = 0;
	uint8_t readDataRes = 0;
	bool result = false;
	if(read_status_reg1(in_out, &readData))
	{
		readDataRes = (readData & (1<<SUS_BIT));
		if(readDataRes)
		result = true;
	}

	return result;
}
/**Checks the WEL bit the status register1
* @return Returns true if the WEL bit is HIGH, else returns false
*/
bool check_WEL_bit(struct io_descriptor *in_out)
{
	uint8_t readData = 0;
	bool result = false;
	// If the status register successfully read, check if the WEL bit is set
	if(read_status_reg1(in_out, &readData))
	{
		readData = (readData & (1<<WEL_BIT));
		if(readData)
		result = true;
	}
	return result;
}

/**Checks the WEL and BUSY bit in the status register1
* @return Returns true if the WEL bit is HIGH and BUSY bit is LOW, else returns false
*/

bool check_WEL_BUSY_bit(struct io_descriptor *in_out)
{
	uint8_t readData = 0;
	uint8_t resWEL = 0, resBusy = 0;
	bool result = false;
	// If the status register read is success
	if(read_status_reg1(in_out, &readData))
	{
		// Check if the WEL bit is set
		resWEL = (readData & (1<<WEL_BIT));
		if(resWEL)
		{
			resBusy = (readData & (1<<BUSY_BIT));
			if (resBusy != 0X01)
			{
				result = true;
			}
		}
	}
	
	return result;
}
/**Writes the data into status register 1
* @param reg_data Contains the data that is to be written into the status register
* @return Returns 0 if the BUSY bit is HIGH or if the WEL bit is 0, else returns one after the data is written
*/
bool write_status_reg1(struct io_descriptor *in_out, uint8_t reg_data)
{
	bool result = false;
	write_enable(in_out);
	uint8_t data_array[3] = {WRITE_STATUS_REG1, reg_data};
	//	printf("reg.c%d\n", reg_data);
	//	printf("busy %d\n", busy_bit);
	
	if(check_WEL_BUSY_bit(in_out))
	{
		gpio_set_pin_level(FLASH_CS, false); //Pull chip select low
		io_write(in_out, (uint8_t*)data_array, 2); //Send the instruction followed by data to the DI line
		gpio_set_pin_level(FLASH_CS, true);// Pull chip select high
		delay_ms(DELAY_WRITE_STATUS_REG);
		result = true;
	}
	return result;
}

/**Writes the data into status register 2
* @param reg_data Contains the data that is to be written into the status register
* @return Returns 0 if the BUSY bit is HIGH or if the WEL bit is 0, else returns one after the data is written
*/
bool write_status_reg2(struct io_descriptor *in_out, uint8_t reg_data)
{
	bool result = false;
	write_enable(in_out);
	uint8_t data_array[3] = {WRITE_STATUS_REG2, reg_data};
	//	printf("reg.c%d\n", reg_data);
	//	printf("busy %d\n", busy_bit);
	
	if(check_WEL_BUSY_bit(in_out))
	{
		gpio_set_pin_level(FLASH_CS, false); //Pull chip select low
		io_write(in_out, (uint8_t*)data_array, 2); //Send the instruction followed by data to the DI line
		gpio_set_pin_level(FLASH_CS, true);// Pull chip select high
		delay_ms(DELAY_WRITE_STATUS_REG);
		result = true;
	}
	return result;
}

/**Writes the data into status register 3
* @param reg_data Contains the data that is to be written into the status register
* @return Returns 0 if the BUSY bit is HIGH or if the WEL bit is 0, else returns one after the data is written
*/
bool write_status_reg3(struct io_descriptor *in_out, uint8_t reg_data)
{
	bool result = false;
	write_enable(in_out);
	uint8_t data_array[3] = {WRITE_STATUS_REG3, reg_data};
	//	printf("reg.c%d\n", reg_data);
	//	printf("busy %d\n", busy_bit);
	
	if(check_WEL_BUSY_bit(in_out))
	{
		gpio_set_pin_level(FLASH_CS, false); //Pull chip select low
		io_write(in_out, (uint8_t*)data_array, 2); //Send the instruction followed by data to the DI line
		gpio_set_pin_level(FLASH_CS, true);// Pull chip select high
		delay_ms(DELAY_WRITE_STATUS_REG);
		result = true;
	}
	return result;
}
/** Stores the data in the specified page on the flash memory
* @param Number_of_data: It is the number of bytes of data that has to be stored in the page
* @param address 24-bit address of the location in the page where the data has to be written
* @param write_data_array[] This array contains the data that has to be stored
* @return Returns  0xFFFFFFFF if the data written fails, returns the next address to be written if the data written is successful
*/

uint32_t page_program(struct io_descriptor *in_out, uint32_t address_data, uint16_t Number_of_data, uint8_t write_data_array[])
{
	uint32_t result = FLASH_ACCESS_ERROR;
	uint16_t ctr = 0;
	write_enable(in_out);
	/*uint32_t check_write = 0;
	uint32_t check_read = 0;*/
	uint8_t address_0 = (uint8_t)(0x000000FF & address_data);
	uint8_t address_1 = (uint8_t)((0x0000FF00 & address_data)>>8);
	uint8_t address_2 = (uint8_t)((0x00FF0000 & address_data)>>16);
	
	/*if(g_hwDebugMode)
	{
		printf("FLASH:address0 in page program: %d\n", address_0);
		printf("FLASH:address1 in page program: %d\n", address_1);
		printf("FLASH:address2 in page program: %d\n", address_2);
	}*/
	
	uint8_t data_array[4] = {PAGE_PRGM, address_2, address_1, address_0};
	//	printf("busy %d\n", (check_BUSY_bit(in_out)));
	/*if(g_hwDebugMode)
	printf("FLASH:no of data in page program:%d\n", Number_of_data);*/
	
	if(check_WEL_BUSY_bit(in_out) && Number_of_data <= 256)
	{
		gpio_set_pin_level(FLASH_CS, false);//Pull chip select low
		io_write(in_out, (uint8_t*)data_array, 4);// Send out the instruction and address on to the DI line
		if(io_write(in_out, (uint8_t*)write_data_array, Number_of_data) == Number_of_data) // Send out the data on to the DI line
		{
			gpio_set_pin_level(FLASH_CS, true);					// Pull chip select high
			delay_ms(DELAY_PAGE_PRGM);							// wait for the write operation to complete
			while(check_BUSY_bit(in_out) && ctr < 60000)		// If it is still busy, give 600ms more - which is more than enough
			{
				delay_us(10);
				ctr++;
			}
			if(ctr < 60000)										// Flash completes the task
			result = address_data + (Number_of_data) ;			// Address of the last written byte
		}
		else
		{
			gpio_set_pin_level(FLASH_CS, true);					//Pull chip select high
			/*if(g_hwDebugMode)
			printf("FLASH:write failed in page program operation\n");*/
		}
	}
	//	printf("xx%ld\n", result);
	return result;
}
/** Reads the data from the specified page in the flash memory
* @param Number_of_data: It is the number of bytes of data that has to be read
* @param address 24-bit address of the location from where the data has to be read
* @param read_data_array[] The read data is stored in this array
* @return 0xFFFFFFFF if the read is failed, returns next address to be read if the read is successful
*/
uint32_t read_data(struct io_descriptor *in_out, uint32_t address_data, uint32_t Number_of_data, uint8_t* read_data_array)
{
	int32_t check_read = 0;
	uint32_t result = FLASH_ACCESS_ERROR;
	uint8_t address_0 = (uint8_t)(0x000000FF & address_data);
	uint8_t address_1 = (uint8_t)((0x0000FF00 & address_data)>>8);
	uint8_t address_2 = (uint8_t)((0x00FF0000 & address_data)>>16);
	
	/*if(g_hwDebugMode)
	{
		printf("FLASH:address0 in read_data: %d\n", address_0);
		printf("FLASH:address1 in read_data: %d\n", address_1);
		printf("FLASH:address2 in read_data: %d\n", address_2);
	}*/
	
	uint8_t data_array[5] = {READ_DATA, address_2, address_1, address_0, 0};

	// The number of data that can be read at a time is limited by the library function capability
	// The io_write and io_read function takes the argument "length" that is of type uint16_t 
	// @ref io_write
	if(Number_of_data <= 65535)
	{
		gpio_set_pin_level(FLASH_CS, false);										// Pull chip select low
		io_write(in_out, (uint8_t*)data_array, 4);									// Send out the instruction and address on to the DI line
		check_read = io_read(in_out, (uint8_t*)read_data_array, Number_of_data);	// Read the data from the DO line
		gpio_set_pin_level(FLASH_CS, true);											// Pull chip select high
		
		// io_read returns the number of data read, if the read is successful
		if(check_read != Number_of_data)
		{
			if(g_hwDebugMode)
			printf("FLASH:read failed- with the count of read data as %ld - count of expected data %ld\n", check_read, Number_of_data);
		}
		else
			result = check_read + address_data;

	}
	else
	{
		if(g_hwDebugMode)
		printf("FLASH: Number of data expected to read is greater than 65535\n");
		
	}
	
/*
	if(g_hwDebugMode)
	printf("FLASH:Next address to be read: %ld\n", result);*/
	
	return result;
}

/** Reads the data from the specified page in the flash memory(operates at highest possible frequency)
* @param Number_of_data: It is the number of bytes of data that has to be
* @param address 24-bit address of the location from where the data has to read
* @param write_data_array[] The read data is stored in this array
*/
void fast_read(struct io_descriptor *in_out, uint32_t address_data, uint8_t Number_of_data, uint8_t read_data_array[])
{
	uint8_t address_0 = (uint8_t)(0x0000ff & address_data);
	uint8_t address_1 = (uint8_t)((0x00ff00 & address_data)>>8);
	uint8_t address_2 = (uint8_t)((0xff0000 & address_data)>>16);
	uint8_t data_array[5] = {FAST_READ, address_2, address_1, address_0, 0};
	gpio_set_pin_level(FLASH_CS, false);//Pull chip select low
	io_write(in_out, (uint8_t*)data_array, 5);// Send out the instruction, address and a dummy byte on to the DI line
	io_read(in_out, (uint8_t*)read_data_array,Number_of_data);// Read the data from the DO line
	gpio_set_pin_level(FLASH_CS, true);//Pull chip select high
}

/**Reads the manufactured and device IDs of the device
* @param array[] The data read is stored in this array
* @returns true, if the manufacturer ID and device ID are read successfully; false, if it fails in io_write or io_read.
*/
bool read_manufacturer_device_ID(struct io_descriptor *in_out, uint8_t array[])
{
	bool result = false;
	uint8_t data_array[5] = {READ_MANUFACTURER_DEVICE_ID, 0x00, 0x00, 0x00,0};
	gpio_set_pin_level(FLASH_CS, false); //Pull chip select low
	if(io_write(in_out, (uint8_t*)data_array, 4)== 4) //Send the instruction on to the DI line
	{
		if (io_read(in_out,(uint8_t*)array, 2) == 2) // Read the data from the DO line
		{
			result = true;
		}
	}
	gpio_set_pin_level(FLASH_CS, true); //Pull chip select high
	return result;
}

/**Reads the 64 bit unique ID of the FLASH chip
* @param array[] The read data is stored in this array
*/
bool readFlashUniqueID(struct io_descriptor *in_out, uint8_t array[])
{
	bool result = false;
	uint8_t data_array[6] = {READ_FLASH_UNIQUE_ID, 0x00, 0x00, 0x00, 0x00, 0};
	gpio_set_pin_level(FLASH_CS, false); //Pull chip select low
	if(io_write(in_out, (uint8_t*)data_array, 5)== 5) //Send the instruction on to the DI line
	{
		if (io_read(in_out,(uint8_t*)array, 8) == 8) // Read the data from the DO line
		{
			result = true;
		}
	}
	gpio_set_pin_level(FLASH_CS, true); //Pull chip select high
	return result;
}



/** Erases 4KB of specified memory sector
* @param address Address of the sector that has to be erased
* @return Returns 0 if the BUSY bit is HIGH or if the WEL bit is 0, else returns 1 after the sector is erased
*/
bool sector_erase4kb(struct io_descriptor *in_out, uint32_t address_data)
{
	bool result = false;
	uint8_t ctr = 0;
	write_enable(in_out);
	uint8_t address_0 = (uint8_t)(0x0000ff & address_data);
	uint8_t address_1 = (uint8_t)((0x00ff00 & address_data)>>8);
	uint8_t address_2 = (uint8_t)((0xff0000 & address_data)>>16);
	uint8_t data_array[4] = {SECTOR_ER_4KB, address_2, address_1, address_0};
	if(check_WEL_BUSY_bit(in_out))
	{
		gpio_set_pin_level(FLASH_CS, false);
		if(io_write(in_out, (uint8_t*)data_array, 4) == 4)
		{
			gpio_set_pin_level(FLASH_CS, true);
			delay_ms(DELAY_SECTOR_ERASE_4KB);
			while (ctr < 10 && check_BUSY_bit(in_out))
			{
				ctr++;
				delay_ms(10);
			}
			if(!check_BUSY_bit(in_out))
			result = true;
		}
		gpio_set_pin_level(FLASH_CS, true);
	}
	return result;
}

/** Erases 32KB of specified memory block
* @param address Address of the block that has to be erased
* @return Returns 0 if the BUSY bit is HIGH or if the WEL bit is 0, else returns 1 after the block is erased
*/
bool block_erase32kb(struct io_descriptor *in_out, uint32_t address_data)
{
	bool result = false;
	uint8_t ctr = 0;
	write_enable(in_out);
	uint8_t address_0 = (uint8_t)(0x0000ff & address_data);
	uint8_t address_1 = (uint8_t)((0x00ff00 & address_data)>>8);
	uint8_t address_2 = (uint8_t)((0xff0000 & address_data)>>16);
	uint8_t data_array[4] = {BLOCK_ER_32KB, address_2, address_1, address_0};
	if(check_WEL_BUSY_bit(in_out))
	{
		gpio_set_pin_level(FLASH_CS, false);
		if(io_write(in_out, (uint8_t*)data_array, 4) == 4)
		{
			gpio_set_pin_level(FLASH_CS, true);
			delay_ms(DELAY_ERASE_32KB);
			while (ctr < 10 && check_BUSY_bit(in_out))
			{
				ctr++;
				delay_ms(100);
			}
			if(!check_BUSY_bit(in_out))
			result = true;
		}
		gpio_set_pin_level(FLASH_CS, true);
	}
	return result;
}


/** Erases 64KB of specified memory block
* @param address Address of the block that has to be erased
* @return Returns 0 if the BUSY bit is HIGH or if the WEL bit is 0, else returns 1 after the block is erased
*/
bool block_erase64kb(struct io_descriptor *in_out, uint32_t address_data)
{
	bool result = false;
	uint8_t ctr = 0;
	write_enable(in_out);
	uint8_t address_0 = (uint8_t)(0x0000ff & address_data);
	uint8_t address_1 = (uint8_t)((0x00ff00 & address_data)>>8);
	uint8_t address_2 = (uint8_t)((0xff0000 & address_data)>>16);
	uint8_t data_array[4] = {BLOCK_ER_64KB, address_2, address_1, address_0};
	if(check_WEL_BUSY_bit(in_out))
	{
		gpio_set_pin_level(FLASH_CS, false);
		if(io_write(in_out, (uint8_t*)data_array, 4) == 4)
		{
			gpio_set_pin_level(FLASH_CS, true);
			delay_ms(DELAY_ERASE_64KB);
			while (ctr < 30 && check_BUSY_bit(in_out))
			{
				ctr++;
				delay_ms(100);
			}
			if(!check_BUSY_bit(in_out))
			result = true;
		}
		gpio_set_pin_level(FLASH_CS, true);
	}
	return result;
}

/** Erases the entire chip memory
* @return Returns 0 if the BUSY bit is HIGH or if the WEL bit is 0, else returns 1 after the chip is erased
*/
bool chip_erase(struct io_descriptor *in_out)
{
	bool result = false;
	uint8_t ctr = 0;
	write_enable(in_out);
	uint8_t data_array[2] = {CHIP_ER, 0};
	if(check_WEL_BUSY_bit(in_out))
	{
		gpio_set_pin_level(FLASH_CS, false);
		if(io_write(in_out, (uint8_t*)data_array, 1) == 1)
		{
			gpio_set_pin_level(FLASH_CS, true);
			delay_ms(DELAY_CHIP_ERASE);
			while (ctr < 100 && check_BUSY_bit(in_out))
			{
				ctr++;
				delay_ms(100);
			}
			if(!check_BUSY_bit(in_out))
			result = true;
		}
		gpio_set_pin_level(FLASH_CS, true);
	}
	return result;
}
/**Suspends the ongoing erase or program operation(except chip erase)
* @return Returns 0 if the BUSY bit is LOW or if the SUS bit is 1, else returns 1 after the operation is complete
*/
bool suspend(struct io_descriptor *in_out)
{
	bool result;
	uint8_t data_array[2] = {SUSPEND, 0};
	if(check_BUSY_bit(in_out) && !(check_SUS_bit(in_out)))
	{
		gpio_set_pin_level(FLASH_CS, false);
		io_write(in_out, (uint8_t*)data_array, 1);
		gpio_set_pin_level(FLASH_CS, true);
		delay_ms(DELAY_SUS);
		while(check_BUSY_bit(in_out))
		{
			delay_ms(1);
		}
		result = 1;
		return result;
	}
	else
	{
		result = 0;
		return result;
	}

}

/**Resumes the suspended operation
* @return Returns 0 if the BUSY bit is HIGH or if the SUS bit is 0, else returns 1 after the operation is complete
*/
bool resume(struct io_descriptor *in_out)
{
	bool result;
	uint8_t data_array[2] = {RESUME, 0};
	if(check_SUS_bit(in_out) && !(check_BUSY_bit(in_out)))
	{
		gpio_set_pin_level(FLASH_CS, false);
		io_write(in_out, (uint8_t*)data_array, 1);
		gpio_set_pin_level(FLASH_CS, true);
		delay_ms(DELAY_RELEASE);
		result = 1;
		return result;
	}
	else
	{
		result = 0;
		return result;
	}
}

/** Makes the device to enter into power down state
* @return true if power down instruction is sent successfully, false otherwise
*/
bool pwr_dwn(struct io_descriptor *in_out)
{
	bool result = false;
	uint8_t data_array[2] = {POWER_DOWN_FLASH, 0};
	gpio_set_pin_level(FLASH_CS, false);//Pull chip select low
	if(io_write(in_out, (uint8_t*)data_array, 1) == 1)//Send the instruction on to the DI line
	{
		gpio_set_pin_level(FLASH_CS, true);//Pull chip select high
		delay_ms(DELAY_TDP);
		result = true;
	}
	gpio_set_pin_level(FLASH_CS, true);//Pull chip select high
	return result;
}
/**Releases the device from power down state
* @return true if power down release instruction is sent successfully, false otherwise
*/
bool pwr_dwn_release(struct io_descriptor *in_out)
{
	bool result = false;
	uint8_t data_array[2] = {RELEASE_POWER_DOWN, 0};
	gpio_set_pin_level(FLASH_CS, false);//Pull chip select low
	if(io_write(in_out, (uint8_t*)data_array, 1) == 1)	//Send the instruction on to the DI line
	{	
		gpio_set_pin_level(FLASH_CS, true);//Pull chip select high
		// Release from power-down will take the time duration of tRES1 (See AC Characteristics - 3 micro sec)
		// before the device will resume normal operation and other instructions are accepted
		// The /CS pin must remain high during the tRES1 time duration.
		delay_ms(DELAY_RELEASE);
		result = true;
	}
	gpio_set_pin_level(FLASH_CS, true);//Pull chip select high
	return result;
}

/** Locks the block or sector of memory starting from a specified address
* @param address: It is the starting address of the block or sector of memory which has to be blocked
* @return Returns 0 if the BUSY bit is HIGH or if the SUS bit is 0, else returns 1 after the operation is complete
*/
bool block_sector_lock(struct io_descriptor *in_out, uint32_t address_data)
{
	bool result = false;
	uint8_t address_0 = (uint8_t)(0x000000FF & address_data);
	uint8_t address_1 = (uint8_t)((0x0000FF00 & address_data)>>8);
	uint8_t address_2 = (uint8_t)((0x00FF0000 & address_data)>>16);
	uint8_t data_array[4] = {BLOCK_OR_SECTOR_LOCK, address_2, address_1, address_0};
	write_enable(in_out);
	if(check_WEL_BUSY_bit(in_out))
	{
		gpio_set_pin_level(FLASH_CS, false);
		io_write(in_out, (uint8_t*)data_array, 4);
		gpio_set_pin_level(FLASH_CS, true);
		uint8_t ct=0;
		while(!result && ct<3)
		{
			ct++;
			if(check_BUSY_bit(in_out))				// Busy bit is set high, until the Flash is busy with the operations
													// with respect to the lastly (recently) received instruction
			delay_ms(1);
			else
			result = true;
		}
	}
	return result;
}



/*calculates the starting address of 32 KB block
* @return Returns the calculated 32KB block address
*/
uint32_t block32KB_address(uint8_t i)
{
	uint32_t result = 0x000000;
	result  = i*0x8000;
	
	return result;
}

/*calculates the starting address of 64 KB block
* @return Returns the calculated 64KB block address
*/
uint32_t block64KB_address(uint8_t i)
{
	uint32_t result = 0x000000;
	
	result  = i*0x10000;
	
	return result;
}


/** Unlocks the locked block or sector of memory starting from a specified address
* @param address: It is the starting address of the block or sector of memory which has to be unlocked
* @return Returns true, if the unlock instruction is sent; false, otherwise.
*/
bool block_sector_unlock(struct io_descriptor *in_out, uint32_t address_data)
{
	bool result = false;
	uint8_t address_0 = (uint8_t)(0x000000FF & address_data);
	uint8_t address_1 = (uint8_t)((0x0000FF00 & address_data)>>8);
	uint8_t address_2 = (uint8_t)((0x00FF0000 & address_data)>>16);
	
	uint8_t data_array[4] = {BLOCK_OR_SECTOR_UNLOCK, address_2, address_1, address_0};
	// A Write Enable instruction must be executed before the device will accept the Individual Block/Sector Lock Instruction
	write_enable(in_out);
	if(check_WEL_BUSY_bit(in_out))
	{	// Make sure the WEL bit is set in the status register
		gpio_set_pin_level(FLASH_CS, false);
		io_write(in_out, (uint8_t*)data_array, 4);
		gpio_set_pin_level(FLASH_CS, true);
		
		uint8_t ct=0;
		while(!result && ct<50)
		{
			ct++;
			if(check_BUSY_bit(in_out))				// Busy bit is set high, until the Flash is busy with the operations 
													// with respect to the lastly (recently) received instruction
				delay_ms(1);
			else 
				result = true;
		}
		
	}
	return result;
}
/** Checks whether the specified block or sector is locked or not
* @param address: It is the starting address of the block or sector of memory which has to be checked
* @return Returns 0 if the BUSY bit is HIGH or if the SUS bit is 0, else returns 1 after the operation is complete
*/

bool read_block_sec_lock(struct io_descriptor *in_out, uint32_t address_data)
{
	bool result = false;
	uint8_t address_0 = (uint8_t)(0x000000FF & address_data);
	uint8_t address_1 = (uint8_t)((0x0000FF00 & address_data)>>8);
	uint8_t address_2 = (uint8_t)((0x00FF0000 & address_data)>>16);
	uint8_t data_array[5] = {READ_BLCK_OR_SECTOR_LOCK, address_2, address_1, address_0, 0};
	
	if(check_BUSY_bit(in_out))
	{
		gpio_set_pin_level(FLASH_CS, false);
		io_write(in_out, (uint8_t*)data_array, 4);
		io_read(in_out, (uint8_t*)&data_array[4], 1);
		gpio_set_pin_level(FLASH_CS, true);
		if(data_array[4] & 0x01)		// If the LSB is high, that block is true
		result = true;
	}
	return result;
}
/** Sets all the block or sector lock bits to 1
* @return Returns false, if the BUSY bit is HIGH or if the SUS bit is 0, else returns 1 after the operation is complete
*/
bool global_block_sec_lock(struct io_descriptor *in_out)
{
	bool result = false;
	write_enable(in_out);
	uint8_t ctr=0;
	while(result == false && ctr < 3)
	{
		ctr++;
		if(check_WEL_BUSY_bit(in_out))
		{
			uint8_t data_array[2] = {GLOBAL_BLOCK_OR_SECTOR_LOCK,0};
			gpio_set_pin_level(FLASH_CS, false);
			io_write(in_out,(uint8_t*)data_array, 1);
			gpio_set_pin_level(FLASH_CS, true);
			result = true;
		}
	}
	
	return result;
}
/** Resets all the block or sector lock bits to 0
* @return Returns 0 if the BUSY bit is HIGH or if the SUS bit is 0, else returns 1 after the operation is complete
*/
bool global_block_sec_unlock(struct io_descriptor *in_out)
{
	bool result = false;
	uint8_t ctr = 0;
	do
	{
		ctr++;
		write_enable(in_out);
		if(check_WEL_BUSY_bit(in_out))
		{
			/*printf("inside check_WEL_BUSY_bit\n");*/
			result = true;
			uint8_t data_array[2] = {GLOBAL_BLOCK_OR_SECTOR_UNLOCK,0};
			gpio_set_pin_level(FLASH_CS, false);
			io_write(in_out,(uint8_t*)data_array, 1);
			gpio_set_pin_level(FLASH_CS, true);
			for(uint8_t i=0; (i<32 && (result == true)); i++)
			{
				if(read_block_sec_lock(in_out, block64KB_address(i)))
				{
					result = false;
				//	printf("i:%d\n", i);
				}
				
			}
		}
	} while (!result && ctr < 3);
	
	return result;
}
/* Enables the reset bit and resets the device
* Once the Reset instruction is accepted, any on-going
* internal operations will be terminated and the device will return to its default power-on state and lose all the
* current volatile settings, such as Volatile Status Register bits, Write Enable Latch (WEL) status,
* Program/Erase Suspend status, Read parameter setting (P7-P0), and Wrap Bit setting (W6-W4).
*/
bool reset(struct io_descriptor *in_out)
{
	bool result;
	uint8_t data_array[2] = {EN_RST,RESET};
	if(!(check_BUSY_bit(in_out)) && !(check_SUS_bit(in_out)))
	{
		gpio_set_pin_level(FLASH_CS, false);
		io_write(in_out,(uint8_t*)data_array, 1);
		gpio_set_pin_level(FLASH_CS, true);
		delay_ms(DELAY_RST);
		gpio_set_pin_level(FLASH_CS, false);
		io_write(in_out,(uint8_t*)&data_array[1], 1);
		gpio_set_pin_level(FLASH_CS, true);
		delay_ms(DELAY_RST);
		result = 1;
		return result;
	}
	else
	{
		result = 0;
		return result;
	}
	
}



/*
uint32_t read_SFDP_reg(struct io_descriptor *in_out, uint8_t read_data_array[])
{
uint32_t result;
uint8_t data_array[3] = {READ_SFDP_REG,0,0};
gpio_set_pin_level(FLASH_CS, false);//Pull chip select low
io_write(in_out, (uint8_t*)data_array, 2); //Send the instruction on to the DI line
io_read(in_out, (uint8_t*)read_data_array, 4);// Read the data from the DO line
gpio_set_pin_level(FLASH_CS, true);// Pull chip select high
result = data_array[1];
return result;
}
*/



/**Stores the sensor data
* @param AG_data This array holds the data which has to be stored
* @param Number_of_bytes is the length of the AG_data array
*/
/*
uint32_t writeIntoFlash(struct io_descriptor *in_out, uint8_t AG_data[],uint32_t Number_of_bytes)
{
	uint32_t result = 0;
	uint8_t number_of_blocks_written_array[3] = {0};
	uint8_t write_block_address[3] = {0};
		uint8_t corruptedBlocksArray[65] = {0};
	//	uint8_t next_block_to_be_sent[3] = {0};
	//uint8_t check_array[30] = {0};
	static uint32_t next_address = 0;
	uint8_t noOfcorruptedBlocks = 0;   // Added for flash self test
	/ *printf("\nOUTSIDE\n");* /
	if(Number_of_bytes == TIME_INFO_LENGTH)
	{
	//	printf("TIME_INFO_LENGTH\n");
		read_data(in_out, NEXT_BLOCK_TO_WRITE_ADDRESS, 1, write_block_address);
		read_data(in_out, CORRUPTED_FLASH_BLOCKS_ADDRESS, 1, corruptedBlocksArray); // Added for FLASH self test
		
		// If Next block to be written is 0xff, then make it 0
		if(write_block_address[0] == 0xFF)
		{
			write_block_address[0] = 0;
		}
		
		// If number of corrupted blocks is 0xff, then make it 0
		if(corruptedBlocksArray[0] == 0xFF)
		{
			corruptedBlocksArray[0] = 0;
		}
		
		noOfcorruptedBlocks = corruptedBlocksArray[0];
		
		if(noOfcorruptedBlocks != 0)
		read_data(in_out, CORRUPTED_FLASH_BLOCKS_ADDRESS, noOfcorruptedBlocks+1, corruptedBlocksArray);// Added for FLASH self test
		
		//Skip the corrupted block
		for(uint8_t ctr=0; ctr<corruptedBlocksArray[0]; ctr++)
		{
			for(uint8_t i=0; i<corruptedBlocksArray[0]; i++)
			{
				if( write_block_address[0] == corruptedBlocksArray[i+1] )
				{
					write_block_address[0]++;
					if(write_block_address[0] > 62)
					{
						write_block_address[0] = 0;
					}
				}
			}
		}		

		uint32_t block_address = block32KB_address(write_block_address[0]);// calculate the address of next block to be written.
		next_address = block_address;
		
		block_sector_unlock(in_out, block_address);
		block_erase32kb(in_out, block_address);
	}
	
	result = write_data(in_out, (next_address), Number_of_bytes, AG_data);// store the incoming data
	next_address = result;
	
	/ *if(g_hwDebugMode)
	printf("FLASH:next address in writeIntoFlash: %ld\n", next_address);* /
	
	//read_data(in_out, (result - 8), 8, check_array);//Read the last 8 bytes of stored data
	
	if(Number_of_bytes == 32)
	{
		/ *printf("\n");
		for (uint8 ctr=0; ctr<32; ctr++)
		{
			printf("%c ", AG_data[ctr]);
		}* /
		if( (AG_data[0] == '/') && (AG_data[1] == '/') && (AG_data[2] == '*') && (AG_data[3] == '*') && (AG_data[4] == '*') && (AG_data[5] == '*') &&  (AG_data[6] == '/') && (AG_data[7] == '/'))
		{
			next_address = 0;
			block_sector_lock(in_out, block32KB_address(write_block_address[0])); //Locks the block after writing the data into the block. Added on oct 26th
			
			read_data(in_out, NEXT_BLOCK_TO_WRITE_ADDRESS, 1, write_block_address);
			
			// If Next block to be written is 0xff, then make it 0
			if( (write_block_address[0]) == 0xFF )
			write_block_address[0] = 0;
				
			//read_data(in_out, NUMBER_OF_BLOCKS_WRITTEN_ADDRESS, 1, number_of_blocks_written_array);
			read_data(in_out, CORRUPTED_FLASH_BLOCKS_ADDRESS, 1, corruptedBlocksArray); // Added for FLASH self test
						
			noOfcorruptedBlocks = corruptedBlocksArray[0];// Added for FLASH self test
			
			if(noOfcorruptedBlocks != 0)
			read_data(in_out, CORRUPTED_FLASH_BLOCKS_ADDRESS, noOfcorruptedBlocks+1, corruptedBlocksArray);// Added for FLASH self test
			
			// If Next block to be written is 0xff, then make it 0
			if( (corruptedBlocksArray[0]) == 0xFF )
			corruptedBlocksArray[0] = 0;

			
			// Resets the next block to be written address to 0 when it reaches 62
			if( (write_block_address[0]) == (62) )
			write_block_address[0] = 0;
			else
			write_block_address[0] = (write_block_address[0] + 1);//increment the next block to write
			
			//Skip the corrupted block
			for(uint8_t ctr=0; ctr<corruptedBlocksArray[0]; ctr++)
			{
				for(uint8_t i=0; i<corruptedBlocksArray[0]; i++)
				{
					if( write_block_address[0] == corruptedBlocksArray[i+1] )
					{
						write_block_address[0]++;
						if(write_block_address[0] > 62)
						{
							write_block_address[0] = 0;
						}
					}
				}
			}
			
		
			write_ID_settings_data(in_out, NEXT_BLOCK_TO_WRITE_ADDRESS, write_block_address, 1);// Updates the NEXT_BLOCK_TO_WRITE pointer
			read_data(in_out, NEXT_BLOCK_TO_WRITE_ADDRESS, 1, write_block_address);
		//	printf("FLASH:In writeIntoFlash, next block to be written is:");
		//	printf("%d\n", write_block_address[0]);
			

		
			if(number_of_blocks_written_array[0] >= (63 - noOfcorruptedBlocks) ) // Edited for FLASH self test 
			number_of_blocks_written_array[0] = 0;
			
			number_of_blocks_written_array[0] = (number_of_blocks_written_array[0] + 1); // increment the number of blocks written
			write_ID_settings_data(in_out, NUMBER_OF_BLOCKS_WRITTEN_ADDRESS, number_of_blocks_written_array,2);// Updates the NUMBER_OF_BLOCKS_WRITTEN pointer
			read_data(in_out, NUMBER_OF_BLOCKS_WRITTEN_ADDRESS, 1, number_of_blocks_written_array);
			if (g_hwDebugMode)
			{
				printf("FLASH:Number blocks written is: %d\n", number_of_blocks_written_array[0]);
			}
			
			
			/ * Just for testing * /
			//uint8_t configDataTemp[10] = {0};
			/ *printf("in writing // **** //: Third location\n");
			pwr_dwn_release(in_out);
			readConfig(in_out,configDataTemp,CONFIG_DATA_LENGTH);
			* // * Just for testing * /
			
		}
		
	}
/ *
	if(g_hwDebugMode)
	printf("result of writeIntoFlash:%ld\n", result);* /
	return result;

}
*/
/*calculates the address of one KB
* @return Returns the calculated One KB address
*/
uint32_t one_KB_address(uint8_t i)
{
	uint32_t result = 0x00000000;
	result  = i*0x0400;
	
	return result;
}

/*calculates the starting address of 4 KB sector
* @return Returns the calculated 4KB sector address
*/
uint32_t sector4KB_address(uint8_t i)
{
	uint32_t result = 0x00000000;
	result  = i*0x1000;
	
	return result;
}



/*Moves the data in last 4KB sector to last second 4KB sector
* @return Returns the next available address if the data move is successful and returns 0xFFFFFFFF otherwise
*/
uint32_t move_data(struct io_descriptor *in_out)
{
	// RFR	global_block_sec_unlock(in_out);
	uint32_t result = FLASH_ACCESS_ERROR;
	uint8_t	data_array[300] = {0};
	block_sector_unlock(in_out, (LAST_4KB_ADDRESS - sector4KB_address(1)));
	if( !sector_erase4kb(in_out, (LAST_4KB_ADDRESS - sector4KB_address(1))) )
	{
		if(g_hwDebugMode)
		printf("FLASH:sector erase failed in move_data\n");
	}
	else
	{
		if(read_data(in_out, LAST_4KB_ADDRESS, NO_OF_ADRESSES_OF_LAST4KB_DATA, data_array) != FLASH_ACCESS_ERROR)
		{
			result = write_data(in_out, (LAST_4KB_ADDRESS - sector4KB_address(1)), NO_OF_ADRESSES_OF_LAST4KB_DATA, data_array);
			if (result == FLASH_ACCESS_ERROR)
			{
				if(g_hwDebugMode)
				printf("FLASH:Move data error - page program error\n");
			}
			
		}
	}
	block_sector_lock(in_out, (LAST_4KB_ADDRESS - sector4KB_address(1)));
	return result;
}

/** Writes ID or settings or pointer data into the last 4KB sector
* @param write_address Address of the data that has to be changed.(# defined address values of SSID, password etc)
* @param data This array holds the data that has to be stored
* @param lengthOfData It is the number bytes of data in the data array
* @return Returns the next available address
*/
bool write_ID_settings_data(struct io_descriptor *in_out, uint32_t write_address, uint8_t data[], uint16_t lengthOfData)
{
	// Removed from Review: RFR	global_block_sec_unlock(in_out);
	bool result = false;
	
	block_sector_unlock(in_out, write_address); //Unlocks the last 4KB sector before writing data into it
	
//	move_data(in_out);// Move the data from Last 4KB sector to another 4KB sector
	
/*
	uint8_t configDataTemp[10] = {0};
	if(g_hwDebugMode)
	{
	//	printf("in write ID: LOCATION 1");
	//	pwr_dwn_release(in_out);
		readConfig(in_out,configDataTemp,CONFIG_DATA_LENGTH);
	}*/
	// To erase also, the block should be unlocked; Here, it is already unlocked.
	sector_erase4kb(in_out, write_address);// erase the Last 4KB sector

	// We are trying to write three times if writing into flash fails.
	for(uint8_t loopCtr=0;((loopCtr<3) && (result == false)); loopCtr++)
	{
		if(write_data(in_out, write_address, lengthOfData, data) == FLASH_ACCESS_ERROR) // Write data fails
		{
			result = false;
		}
		else   // Write into flash success
		{
	//		if(copy_data_back_to_last_sector(in_out, write_address))// Copy data from the last second 4KB sector back to last 4KB sector
			result = true;
		}
	}

		block_sector_lock(in_out, write_address); //locks the last 4KB sector after writing data into it
		return result;

}

/**Writes data into flash
* @param address_data It is the address where the data has to be written
* @param number_of_bytes It is the number of bytes of data that has to be written
* @param data_array[] this array holds the data that has to be written
* @return Returns the next available address if the write is successful, 0xFFFFFFFF otherwise
* Make sure to unlock the sector or block (into which data is being written) before using this function.
*/
uint32_t write_data(struct io_descriptor *in_out, uint32_t address_data, uint32_t number_of_bytes, uint8_t data_array[])
{
	//	global_block_sec_unlock(in_out);
	uint8_t address_0 = (uint8_t)(0x000000FF & address_data);		// If the address (where to start writing) matches with the starting address of the page, address_0 = 0
	//printf("address_0:%d\n", address_0);
	uint32_t result = FLASH_ACCESS_ERROR;
	uint16_t numberOfBytesInStartingPage = 0;
	uint16_t numberOfBytesInLastPage = 0;
	uint8_t noOfCompletePages = 0;
// 	
// 	if (address_data == NUMBER_OF_BLOCKS_WRITTEN_ADDRESS)
// 	{
// 		/*if(g_hwDebugMode)
// 		printf("FLASH:data came in data_array[] in write_data - address: %d - %ld\n", data_array[0],address_data );*/
// 	}
	
	// Goes into this condition if the starting address of write is the starting address of page and if the number of bytes that has to be written is less than 256
	// Example: If the address (where to start writing) matches with the starting address of the page, and if the number of bytes
	// to be written is 200, numberOfBytesInStartingPage = 200 and it is written into that page in the below if condition only.
	if((address_0 == 0) && (number_of_bytes < 256))
	{
		numberOfBytesInStartingPage = number_of_bytes;
		numberOfBytesInLastPage = 0;
		noOfCompletePages = 0;
		result = page_program(in_out, address_data, numberOfBytesInStartingPage, data_array);
	}
	
	// Goes into this condition if the starting address of write is the starting address of page and if the number of bytes that has to be written is more than or equal to 256
	// Example: If the address (where to start writing) matches with the starting address of the page, and if the number of bytes
	// to be written is 270, --> noOfCompletePages = 270/256 = 1, numberOfBytesInLastPage = 270 - 256 = 14. The 1st 256 data are written into
	// flash in the "for" loop "for(j=0; j<noOfCompletePages; j++)". Remaining 14 data are written into flash in 
	// the "if" condition - "if(numberOfBytesInLastPage != 0 )"
	if((address_0 == 0) && (number_of_bytes >= 256))
	{
		numberOfBytesInStartingPage = 0;
		noOfCompletePages = (number_of_bytes)/256;
		numberOfBytesInLastPage = number_of_bytes - (noOfCompletePages*256);
	}
	
	// Goes into this condition if the starting address of write is not the starting address of page and if the number of bytes that has to be written is less than 256
	// Example: If the address (where to start writing - 27) does not match with the starting address of the page and if the number of bytes 
	// to be written is 231, --> numberOfBytesInStartingPage = 229, noOfCompletePages = (231 - 229)/ 256 = 0, numberOfBytesInLastPage = 231 - 229 = 2.
	// The 1st 229 data are written into flash in the "if(address_0 != 0)"
	// Remaining data are written into flash in the condition "if(numberOfBytesInLastPage != 0 )"
	if((address_0 != 0x00) && (number_of_bytes < 256))
	{
		numberOfBytesInStartingPage = (0x100 - address_0);				// Space available in the starting page
		if(numberOfBytesInStartingPage > number_of_bytes)				// If the number of data to be written to, is less than the space available
		{
			numberOfBytesInStartingPage = number_of_bytes;
		}
		
		noOfCompletePages = (number_of_bytes - numberOfBytesInStartingPage)/256;				// noOfCompletePages = 0 as number_of_bytes < 256
		//printf("%d\n", noOfCompletePages);
		numberOfBytesInLastPage = number_of_bytes - (noOfCompletePages*256) - numberOfBytesInStartingPage ;
	}
	
	// Goes into this condition if the starting address of write is not the starting address of page and if the number of bytes that has to be written is more than 256
	// Example: If the address (where to start writing - 27) does not match with the starting address of the page and if the number of bytes 
	// to be written is 531, --> numberOfBytesInStartingPage = 256 - 27 = 229, noOfCompletePages = (531 - 229) / 256 = 1, 
	// numberOfBytesInLastPage = 531 - (256) - 229 = 46.
	// The 1st 229 data are written into flash in the "if(address_0 != 0)"
	// The next 256 data are written into flash in the "for" loop "for(j=0; j<noOfCompletePages; j++)".
	// Remaining data are written into flash in the condition "if(numberOfBytesInLastPage != 0 )"
	if((address_0 != 0) && (number_of_bytes >= 256))
	{
		numberOfBytesInStartingPage = (0x100 - address_0);
		noOfCompletePages = (number_of_bytes - numberOfBytesInStartingPage)/256;
		//	printf("%d\n", noOfCompletePages);
		numberOfBytesInLastPage = number_of_bytes - (noOfCompletePages*256) - numberOfBytesInStartingPage;
	}

	uint32_t j = 0;
	if(address_0 != 0) // Goes into this condition if the starting address of write is not the starting address of page
	{
		result = page_program(in_out, address_data, numberOfBytesInStartingPage, data_array);
	}

	
	for(j=0; j<noOfCompletePages; j++)
	{
		//	printf("%d\n", j);
		//	printf("in first page%d\n", numberOfBytesInStartingPage);
		result = page_program(in_out, (address_data + numberOfBytesInStartingPage + 256*j), 256, &data_array[numberOfBytesInStartingPage + j*256] );
		//		printf("%d\n", address_data + numberOfBytesInStartingPage + 256*j);
		//	printf("result:%d\n", result);
	}
	
	
	if(numberOfBytesInLastPage != 0 )
	{
		result = page_program(in_out, address_data + (number_of_bytes - numberOfBytesInLastPage), numberOfBytesInLastPage, &data_array[number_of_bytes - numberOfBytesInLastPage] );
	}
	
	/*if(g_hwDebugMode)
	printf("FLASH:write data result: %ld\n", result);*/
	//printf("result :%ld\n",result);
	return result;
}

/*

uint32_t  write_next_address_data(struct io_descriptor *in_out, uint8_t Adata[])
{
global_block_sec_unlock(in_out);
uint32_t result=0;

sector_erase4kb(in_out,(block64KB_address(31) + sector4KB_address(13)) );

result = write_data(in_out, (block64KB_address(31) + sector4KB_address(13)), 3, Adata );
return result;
}

uint32_t  write_next_read_address(struct io_descriptor *in_out, uint8_t Adata[])
{
global_block_sec_unlock(in_out);
uint32_t result=0;

sector_erase4kb(in_out,(block64KB_address(31) + sector4KB_address(12)) );

result = write_data(in_out, (block64KB_address(31) + sector4KB_address(12)), 3, Adata );
return result;
}*/





/** Sends the stored sensor data
* @param set_num is the set number of the sensor data
* @param Data is the required data(X, Y or Z)
* @param send_array The read data is stored in this array
* @return result returns the number of bytes read from flash.
*/


/*
uint16_t readFromFlash(struct io_descriptor *in_out, uint8_t set_num, uint8_t Data, uint8_t send_array[])
{
	uint16_t result = 0;
	uint8_t number_of_blocks_written_array[3] = {0};
	uint8_t next_block_to_be_sent[3] = {0};
	uint32_t block_address = 0;
	uint8_t count_array[15] = {0};
	static uint32_t next_address = 0;
	static uint32_t count = 0;
	uint8_t corruptedBlocksArray[65] = {0};
	uint8_t noOfcorruptedBlocks = 0;
	
	//	printf("count:%ld\n", count);
	
	read_data(in_out, NUMBER_OF_DATASETS_WRITTEN_ADDRESS, 1, number_of_blocks_written_array);
	//	printf("no of blocks%d\n", number_of_blocks_written_array[0]);
	if(number_of_blocks_written_array[0] >= 1)
	{
		
		// calculates the block address of the data which has to be sent
		read_data(in_out, NEXT_DATA_TO_BE_SENT_WRITE, 1, next_block_to_be_sent);
		/ *if(g_hwDebugMode)
		printf("FLASH:Next block to be sent:%d\n", next_block_to_be_sent[0]);* /

		read_data(in_out, CORRUPTED_FLASH_BLOCKS_ADDRESS, 65, corruptedBlocksArray);// Added for FLASH self test
		noOfcorruptedBlocks = corruptedBlocksArray[0];// Added for FLASH self test
		
		//Skip corrupted blocks
		for(uint8_t i=0; i<noOfcorruptedBlocks; i++)
		{
			if(next_block_to_be_sent[0] == corruptedBlocksArray[i+1])
			next_block_to_be_sent[0]++;
		}

		block_address = block32KB_address(next_block_to_be_sent[0]);
		//	printf("block address: %ld", block_address);
		// calculates the count of x/y/z data
		


		if( (set_num == 1) && (Data == TIME_DATA))
		{
			next_address = read_data(in_out, block_address, TIME_INFO_LENGTH, send_array);
			/ *if(g_hwDebugMode)
			printf("FLASH:time address in readFromFlash:%ld\n", next_address);* /
			result = TIME_INFO_LENGTH;
			/ *read_data(in_out, (next_address - 11), 10, check_array1);* /
			/ *if(g_hwDebugMode)
			for(uint8_t i=0; i<10; i++)
			printf("send array:%d\n", send_array[TIME_INFO_LENGTH - 10 + i]);* /
			
			if( (send_array[TIME_INFO_LENGTH - 10] == 'e') && (send_array[TIME_INFO_LENGTH - 9] == 's') && (send_array[TIME_INFO_LENGTH - 8] == 'm') \
			&& (send_array[TIME_INFO_LENGTH - 7] == 'e') && (send_array[TIME_INFO_LENGTH - 6] == 'b') && (send_array[TIME_INFO_LENGTH - 5] == 'n') \
			&& (send_array[TIME_INFO_LENGTH - 4] == 'e') && (send_array[TIME_INFO_LENGTH - 3] == 's') && (send_array[TIME_INFO_LENGTH - 2] == 'd') \
			&& (send_array[TIME_INFO_LENGTH - 1] == 'e'))
			{
			/ *	if(g_hwDebugMode)
				printf("embedsense received in time info\n");* /
			}
			else
			{
				result = 0xffff;
				
				if(g_hwDebugMode)
					printf("embedsense not received in time info\n");
			}
			
		}
	
		/ *
		else if(Data == CLOSE)
		{
		//				printf("check close");
		next_address = read_data(in_out, (next_address), 5, send_array);
		//			printf("close address read:%ld\n", next_address);
		
		//	read_data(in_out, (next_address - 5), 5, check_array2);
		if( (send_array[0] == 'c') && (send_array[1] == 'l') && (send_array[2] == 'o') && (send_array[3] == 's') && (send_array[4] == 'e') )
		{
		result = 5;
		if(g_hwDebugMode)
		printf("close received\n");
		}
		else
		{
		result = 0xffff;
		if(g_hwDebugMode)
		printf("close not received\n");
		}
		
		}* /
		
		else if(Data == END_STRING)
		{
			next_address = read_data(in_out, (next_address), 32 , send_array);
			if( (send_array[0]=='/') && (send_array[1]=='/') && (send_array[2]=='*') && (send_array[3]=='*') && (send_array[4]=='*') && (send_array[5]=='*') && (send_array[6]=='/') && (send_array[7]=='/') )
			{
				result = 32;
			}	
			else
			{
				result = 0xffff;
			}	
			
		}
		else
		{
			result = 0xffff;
			if(g_hwDebugMode)
			printf("FLASH:wrong data requested, only time data,x data, y data, z data and end string can be read\n");
		}
	}
	else
	{
		result = 0xffff;
		if(g_hwDebugMode)
		printf("FLASH:No data to be sent in flash, cannot update pointers");
	}
	
	return result;
}*/

/**	Updates the pointers necessary for Flash access
*	After sending the data, the "NUMBER_OF_BLOCKS_WRITTEN_ADDRESS" should be decremented
*	and the "NEXT_DATA_TO_BE_SENT" should be moved to the next dataset
*/

void update_pointers(struct io_descriptor *in_out)
{
	uint8_t number_of_blocks_written_array[3] = {0};
	uint8_t next_block_to_be_sent[3] = {0};
	uint8_t corrupted_FLASH_blocks_array[66] = {0};
	uint8_t read_array[4] = {0};
	
	read_data(in_out, NUMBER_OF_DATASETS_WRITTEN_ADDRESS, 1, number_of_blocks_written_array);
	read_data(in_out, NEXT_DATA_TO_BE_SENT_WRITE, 1, next_block_to_be_sent);
	read_data(in_out, CORRUPTED_FLASH_BLOCKS_ADDRESS, 65, corrupted_FLASH_blocks_array);

	if(number_of_blocks_written_array[0] != 0)
	{
		number_of_blocks_written_array[0]--;
		if (number_of_blocks_written_array[0] > 63 )
		{
			number_of_blocks_written_array[0] = 0;
		}
		
		
		if (next_block_to_be_sent[0] >= 63 )
		{
			next_block_to_be_sent[0] = 0;
		}
		next_block_to_be_sent[0]++;
		
		//Skip the corrupted block
		for(uint8_t ctr=0; ctr<corrupted_FLASH_blocks_array[0]; ctr++)
		{	
			for(uint8_t i=0; i<corrupted_FLASH_blocks_array[0]; i++)
			{
				if( next_block_to_be_sent[0] == corrupted_FLASH_blocks_array[i+1] )
				{
					next_block_to_be_sent[0]++; 
					if(next_block_to_be_sent[0] > 62) 
					{
						next_block_to_be_sent[0] = 0;
					}
				}
			}
		}
		
	//	printf("FLASH:Next block to be sent before:%d\n", next_block_to_be_sent[0]);				
		write_ID_settings_data(in_out, NUMBER_OF_DATASETS_WRITTEN_ADDRESS, number_of_blocks_written_array, 1);
		write_ID_settings_data(in_out, NEXT_DATA_TO_BE_SENT_WRITE, next_block_to_be_sent, 1);
		
		
		if(g_hwDebugMode)
		{
			read_data(in_out, NUMBER_OF_DATASETS_WRITTEN_ADDRESS, 1, read_array);
			read_data(in_out, NEXT_DATA_TO_BE_SENT_WRITE, 1, &read_array[1]);
			
	//		printf("FLASH:number of blocks written:%d\n", read_array[0]);
	//		printf("FLASH:Next block to be sent:%d\n", read_array[1]);
		}
	}
	else
	{
		if(g_hwDebugMode)
		printf("FLASH:No data to be sent in flash, cannot update pointers\n");
	}
//	printf("update pointers done\n");
}


void makeFlashReadyForWrite(struct io_descriptor *ioFlash)
{
//	pwr_dwn_release(ioFlash);
	write_enable(ioFlash);
	write_enable_volatile_reg(ioFlash);
	
	write_status_reg1(ioFlash, STATUS_REG1_DATA);
	write_status_reg2(ioFlash, STATUS_REG2_DATA);
	write_status_reg3(ioFlash, STATUS_REG3_DATA);	// Output driver strength is set as 100% and WPS is 1
	// WPS - Write Protect Selection; When WPS=0, the device will use the combination of CMP, SEC, TB, BP[2:0] bits 
	// to protect a specific area of the memory array. When WPS=1, the device will utilize the Individual Block Locks to 
	// protect any individual sector or blocks. The default value for all Individual Block Lock bits is 1 upon device power on or after reset.
}



void unlockSectorsInFirstAndLastBlock(struct io_descriptor *in_out)
{
	uint8_t nextBlockToWriteArray[3] = {0};
	read_data(in_out, NEXT_WRITE_ADDRESS, 1,  nextBlockToWriteArray);
	if(nextBlockToWriteArray[0] == 255)
		nextBlockToWriteArray[0] = 0;

	if(nextBlockToWriteArray[0] == 0 || nextBlockToWriteArray[0] == 1 || nextBlockToWriteArray[0] == 62)
	{
		
		for(uint8_t loopCt=0; loopCt<8; loopCt++)
		{
			block_sector_unlock( in_out, (block32KB_address(nextBlockToWriteArray[0]) + sector4KB_address(loopCt)) );
		}
	}
}


bool flashSelfTest(struct io_descriptor *in_out)
{
	bool result = true;
	uint32_t counter = 0;


	uint8_t corruptedBlocksArray[65] = {0};
	
	//	delay_ms(5000);
/*
	//First location in this address has the number of corrupted blocks. This is stored in the first location of corruptedBlocksArray
	read_data(in_out, CORRUPTED_FLASH_BLOCKS_ADDRESS, 1, corruptedBlocksArray);*/
	
	if(global_block_sec_unlock(in_out))
	{	
		uint8_t dataArray[260] = {0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA};
		uint8_t dataArray1[260] = {0};
			
		delay_ms(50);
		if (g_hwDebugMode)
		printf("FLASH self test started\n");
		//This for-loop performs self test on 63 blocks of FLASH(except the last block)
		for(uint8_t blockCtr=0; blockCtr<=62; blockCtr++) //63x32KB blocks
		{
			block_erase32kb(in_out, block32KB_address(blockCtr)); // Erase the block before testing
			for(uint8_t pageCtr=0; (pageCtr < 128) ; pageCtr++)//128 pages in each 32KB block
			{
				write_data(in_out, (blockCtr*32768 + pageCtr*256), 256, dataArray); // Write data into flash
				read_data(in_out, (blockCtr*32768 + pageCtr*256), 256, dataArray1); // Read the data back 
				//Check if the data read from the FLASH is correct
				for(uint16_t j=0; j<256; j++)  //256 bytes in each page
				{
					if(dataArray1[j] == 0xAA)
					{
						counter++;  //Increment this counter if the data that is read is correct
					}
					else
					{
						if (g_hwDebugMode)
						{
							//printf("%d\n", dataArray1[j]);
							printf("location:%ld\n", ((uint32_t)(blockCtr*32768 + pageCtr*256 + j)));
						}
						
					}
				
				}

			}
			
			// Each block has 32768 locations, if any location is corrupted, the counter value will not be equal to 32768
			if(counter != 32768)  
			{
				corruptedBlocksArray[0]++;// Increment the number of corrupted blocks

				corruptedBlocksArray[corruptedBlocksArray[0]] = blockCtr; // Store the address of corrupted block in the array
				if(g_hwDebugMode)
				{
					printf("Number of corrupted blocks:%d\n", corruptedBlocksArray[0]);
					for (uint8_t i=0; i<corruptedBlocksArray[0]; i++)
					{
						printf("The corrupted blocks are:%d", corruptedBlocksArray[1+i]);
					}
					printf("\n");
				}
				
				updateFLASHcorruptedBlocks(in_out, corruptedBlocksArray); // Write the corrupted block's address into FLASH
			}
			
			// If more than 20 blocks are corrupted in FLASH, inform the gateway that FLASH has gone bad
			if(corruptedBlocksArray[0] > 20)
			{
				result = false;
			}
			counter = 0;		
		}
		
		global_block_sec_lock(in_out);	
	}
	
	else
	{
		result = false;
		if (g_hwDebugMode)
		printf("global_block_sec_unlock failed in flash test\n");
	}
/*
	
	if(result == true)
	{
		result = testLastBlockOfFLASH(in_out);
	}

*/


	return result;
}



/** Writes ID or settings or pointer data into the last 4KB sector
* @param write_address Address of the data that has to be changed.(# defined address values of SSID, password etc)
* @param data This array holds the data that has to be stored
* @param lengthOfData It is the number bytes of data in the data array
* @return Returns the next available address
*/
void updateFLASHcorruptedBlocks(struct io_descriptor *in_out, uint8_t data[])
{

	uint8_t numberOfCorruptedBlocks = 0;
	numberOfCorruptedBlocks = data[0];
	block_sector_unlock(in_out, CORRUPTED_FLASH_BLOCKS_ADDRESS); //Unlocks the last 4KB sector before writing data into it

	// To erase also, the block should be unlocked; Here, it is already unlocked.
	sector_erase4kb(in_out, CORRUPTED_FLASH_BLOCKS_ADDRESS);// erase the Last 4KB sector

	write_data(in_out, CORRUPTED_FLASH_BLOCKS_ADDRESS, 1, data);
	write_data(in_out, CORRUPTED_FLASH_BLOCKS_ADDRESS, 1, &data[numberOfCorruptedBlocks]);

	block_sector_lock(in_out, CORRUPTED_FLASH_BLOCKS_ADDRESS); //locks the last 4KB sector after writing data into it
}


/*
bool testLastBlockOfFLASH(struct io_descriptor *in_out)
{
	uint8_t dataArray[260] = {0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA};
	uint8_t dataArray1[260] = {0};
	bool result = false;
	uint32_t counter = 0;
	uint32_t address = 0;
	uint32_t addressOfLastBlock = block32KB_address(63);
	block_sector_unlock(in_out, addressOfLastBlock);
	// Counter in for-loop starts from 64 because SSID, PWD, Sensor config and FLASH corrupted blocks data should not be included in self test
	for(uint8_t pageCtr=64; (pageCtr < 128) ; pageCtr++)//128 pages in each 32KB block
	{
		address = pageCtr*256+addressOfLastBlock; // Find the starting address of each page
	//	printf( "address:%d\n", address);
		// If starting address of the page matches with the starting address of the 4KB sector, erase the sector
		if((address & 0x000FFF) == 0) 
		{
			block_sector_unlock(in_out, address);
			sector_erase4kb(in_out, address);
			if(g_hwDebugMode)
			printf("sector_erase4kb address in testLastBlockOfFLASH:%ld\n", address);
		}

		write_data(in_out, (addressOfLastBlock + pageCtr*256), 256, dataArray);
		read_data(in_out, (addressOfLastBlock + pageCtr*256), 256, dataArray1);
		

		for(uint16_t j=0; j<256; j++)  //256 bytes in each page
		{
			if(dataArray1[j] == 0xAA)
			{
				counter++;
			}
			else
			{
				
			//	printf("%d\n", dataArray1[j]);
			//	printf("FLASH:Malfunctioning location:%ld\n", (addressOfLastBlock + pageCtr*256 + j));
			}
		}
	}
	// Erase the sector after self test
	for(uint8_t ctr=0; ctr<4; ctr++)
	{
		sector_erase4kb(in_out, (0x1FC000+ctr*0x1000) );
		block_sector_lock(in_out, (0x1FC000+ctr*0x1000) );
	}
			
	if(counter == 16384)
	{
		result = true;
		if (g_hwDebugMode)
		printf("FLASH:'I am good'\n");
	}

	block_sector_lock(in_out, addressOfLastBlock);
	
	return result;
}*/


/*Function Name: writeIntoFlash
* Description	: It will write the data into required flash locations 
* Params		: Flash descriptor, write_address, NoofBytes and Data
* Returns		: This function will returns the next address location into which data has to be stored. **/
uint32_t writeIntoFlash(struct io_descriptor *in_out,uint32_t write_Address,uint32_t NoOfBytes,uint8_t data[])
{
	uint32_t nextAddress = 0;
	uint32_t flashOverflowAddress = MAX_USABLE_FLASH_ADDRESS;
	uint32_t spaceLeftInFlash = 0;
	
//	printf("flashOverflowAddress :%ld\n",flashOverflowAddress);
	
	
	if(flashOverflowAddress>= write_Address)
	{
		spaceLeftInFlash = flashOverflowAddress - write_Address;
	}
	else
	{
		spaceLeftInFlash = write_Address - flashOverflowAddress;
	}


	//If address exceeds the maximum limit, reset it to 0
	if (NoOfBytes > spaceLeftInFlash)
	{
		write_Address = 0;
	}
	if ((block_sector_unlock(in_out, write_Address)) && (block_sector_unlock(in_out, write_Address + NoOfBytes)) )
	{
		
		nextAddress = write_data(in_out,write_Address,NoOfBytes,data);
		write_Address = nextAddress ;
		//printf("nextAddress : %ld\n",nextAddress);
	}
	
	//Lock the blocks after writing data into FLASH
	block_sector_lock(in_out, (write_Address-NoOfBytes));
	block_sector_lock(in_out, write_Address);
	
	return nextAddress;
}

/*Function Name	: readFromFlash
* Description	: It will write the data into required flash locations
* Params		: Flash descriptor, read_Address, NoofBytes and Data
* Returns		: This function will returns the next address location from which data has to be read. **/
uint32_t readFromFlash(struct io_descriptor *in_out,uint32_t read_Address,uint32_t NoOfBytes,uint8_t *data)
{	
	uint32_t next_address = 0;
	uint32_t spaceLeftInFlash = 0;

	if(flashOverflowAddress >= read_Address)
	{
		spaceLeftInFlash = flashOverflowAddress - read_Address;
	}
	else
	{
		spaceLeftInFlash = read_Address - flashOverflowAddress;
	}

	if (NoOfBytes > spaceLeftInFlash)
	{
		read_Address = 0;
		printf("readAddress:%ld\n", read_Address);
	}
	
	if ((block_sector_unlock(in_out, read_Address)) && (block_sector_unlock(in_out, read_Address + NoOfBytes)) )
	{
		
		next_address = read_data(in_out,read_Address,NoOfBytes,data);
		read_Address = next_address ;
	}
		
	//Lock the blocks after writing data into FLASH
	block_sector_lock(in_out, read_Address);
	block_sector_lock(in_out, (read_Address-NoOfBytes));
	
	
	return next_address;

}

/*Function Name	: writeNextAdrressIntoFlash
* Description	: It is used to store the next address into which data has to write/read
* Params		: Flash descriptor, Address, NoofBytes and locationOfAddress
* Returns		: This function will returns true if writing into flash is success otherwise false. **/
bool writeNextAdrressIntoFlash(struct io_descriptor *in_out,uint32_t Address,uint32_t locationOfAddress)
{
	bool result  = false;
	
	//Store next address to be written into flash
	uint8_t nextWriteAddress[5] = {0};
	nextWriteAddress[3] = (uint8_t)(Address >> 0 )& 0xFF;
	nextWriteAddress[2] = (uint8_t)(Address >> 8 )& 0xFF;
	nextWriteAddress[1] = (uint8_t)(Address >> 16)& 0xFF;
	nextWriteAddress[0] = (uint8_t)(Address >> 24)& 0xFF;
	
// 	for (uint8_t i=0;i<4;i++)
// 	{
// 		printf("%d\n",nextWriteAddress[i]);
// 	}
	//printf("writeAddress while storing old data:%ld\n",write_Address);
	
	if ((block_sector_unlock(in_out, locationOfAddress)))
	{
		sector_erase4kb(in_out, locationOfAddress);
		
		for(uint8_t loopCtr=0;((loopCtr<3) && (result == false)); loopCtr++)
		{
			if(write_data(in_out,locationOfAddress,4,nextWriteAddress) == FLASH_ACCESS_ERROR)
			{
				result = false;
			}
			else
			{
				result = true;
// 				delay_us(10);
// 				read_data(in_out,locationOfAddress,4,&nextWriteAddress[0]);
// 				for (uint8_t i=0;i<4;i++)
// 				{
// 					printf("%d",nextWriteAddress[i]);
// 				
// 				}
			}
		}
		
	
	}		
	
	block_sector_lock(in_out, locationOfAddress);
	
	return result;
}
/*Function Name : readNextWriteorReadAddress
* Description	: This function returns the address of location from where data has to be stored/read.
* Params		: Flash descriptor, address(in which location address had stored)
**/
uint32_t readNextWriteorReadAddress(struct io_descriptor *in_out,uint32_t Address)
{
	uint8_t readArray[4] = {0};	
	uint32_t nextAddress = 0;
	block_sector_unlock(in_out,Address);
	
	if (read_data(in_out,Address,4,&readArray[0]) != FLASH_ACCESS_ERROR)
	{
		nextAddress = /*(uint32_t)((readArray[0] << 24) & 0xFF000000) |*/(uint32_t)((readArray[1] << 16) & 0x00FF0000) | (uint32_t)((readArray[2] << 8) & 0x0000FF00) | (uint32_t)(readArray[3] & 0x000000FF) ;
	}
	
	block_sector_lock(in_out, Address);	
	
	return nextAddress;
	
}


/*Function Name : storeNoOfDataSetsInFlash
* Description	: This function is helpful to store the number of data sets are collected till now
* Params		: Flash descriptor, write_Address,datasets
**/

void storeNoOfDataSetsInFlash(struct io_descriptor *in_out,uint32_t write_Address,uint16_t datasets)
{
	
	uint8_t dataSetsArray[3] = {0};
	
	block_sector_unlock(in_out,write_Address);
	
	sector_erase4kb(in_out,write_Address);
	
	dataSetsArray[1] = (uint8_t) (datasets >> 0)& 0xFF;
	dataSetsArray[0] = (uint8_t) (datasets >> 8)& 0xFF;
	
// 	dataSetsArray[1] = (uint8_t) (datasets >> 0)& 0xFF;
// 	dataSetsArray[0] = (uint8_t) (datasets >> 8)& 0xFF;
	
	
// 	if(g_hwDebugMode)
// 	printf("dataSetsArray[0] :%ld\n",dataSetsArray[0]);
// 	printf("dataSetsArray[1] :%ld\n",dataSetsArray[2]);
	
	write_data(in_out,write_Address,2,&dataSetsArray[0]);
	
	block_sector_lock(in_out, write_Address);

}


/*Function Name : NoOfDataSetsRedFromFlash
* Description	: This function returns the number of data sets are stored till now
* Params		: Flash descriptor, write_Address,datasets
**/

uint16_t NoOfDataSetsRedFromFlash(struct io_descriptor *in_out, uint32_t dataSetsReadAddress)
{
	uint16_t noOfDataSetsStored = 0;
	uint8_t readArray[3] = {0};
	
	block_sector_unlock(in_out,dataSetsReadAddress);
	
	if (read_data(in_out,dataSetsReadAddress,2,&readArray[0]) != FLASH_ACCESS_ERROR)
	{
		noOfDataSetsStored = /*(uint32_t)((readArray[0] << 24) & 0x00FF0000) |*//*(uint16_t)((readArray[1] << 16) & 0x00FF0000) |*/ (uint16_t)((readArray[0] << 8) & 0xFF00) | (uint16_t)((readArray[1]  << 0 )& 0x00FF) ;
		
		if (noOfDataSetsStored  == 0xFFFF)
		{
			noOfDataSetsStored = 0;
			//printf("Initial NO of Data sets :%ld\n",noOfDataSetsStored);
 		}
		
	}
	else
	noOfDataSetsStored = 0xFFFF;
	
	block_sector_lock(in_out, dataSetsReadAddress);
	
	return noOfDataSetsStored;
	
}


/* RFR

void  copy_data_back_to_last_sector(struct io_descriptor *in_out, uint32_t data_to_be_changed)
{
for (uint8_t i=0; i<8; i++)
{
if(addressArray[i] != data_to_be_changed)
{
read_data()
write_data()
}
}
}

*/

/* if(write_address == PASSWORD_ADDRESS)
{

}


else if(write_address == SSID_ADDRESS)
{
result =  write_data(in_out, SSID_ADDRESS, lengthOfData, data);
}



else if(write_address == SENSOR_CONFIGURATIONS_ADDRESS)
result =  write_data(in_out, SENSOR_CONFIGURATIONS_ADDRESS, lengthOfData, data);


else if(write_address == TIME_INFO_ADDRESS)
result =  write_data(in_out, TIME_INFO_ADDRESS, lengthOfData, data);


else if(write_address == NUMBER_OF_BLOCKS_WRITTEN_ADDRESS)
{
result =  write_data(in_out, NUMBER_OF_BLOCKS_WRITTEN_ADDRESS, lengthOfData, data);
}


else if(write_address == NEXT_BLOCK_TO_WRITE_ADDRESS)
{
result =  write_data(in_out, NEXT_BLOCK_TO_WRITE_ADDRESS, lengthOfData, data);
}


else if(write_address == NEXT_DATA_TO_BE_SENT)
result =  write_data(in_out, NEXT_DATA_TO_BE_SENT, lengthOfData, data);


else if(write_address == PREVIOUSLY_ERASED_BLOCK_ADDRESS)
result =  write_data(in_out, PREVIOUSLY_ERASED_BLOCK_ADDRESS, lengthOfData, data);


else{}
RFR */


