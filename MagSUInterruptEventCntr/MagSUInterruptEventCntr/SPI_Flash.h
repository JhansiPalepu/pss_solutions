/*
* SPI_Flash.h
*
* Created: 8/3/2017 10:26:23 AM
*  Author: Ramachandra
*/

#include <stdbool.h>
#include "winc1500/driver/include/m2m_types.h"
#ifndef SPI_FLASH_H_
#define SPI_FLASH_H_

#define  FLASH_ACCESS_ERROR 0xFFFFFFFF

// Status register 1 bits
#define BUSY_BIT 0
#define WEL_BIT 1
#define BP0_BIT 2
#define BP1_BIT 3
#define BP2_BIT 4
#define TB_BIT 5
#define SEC_BIT 6



//Status register 2 bits
#define SUS_BIT 7
#define CMP_BIT 6



//Status register 3 bits
#define WPS_BIT 2
#define DRV1 6
#define DRV2 5



// instruction codes
#define WRITE_ENABLE 0x06
#define WR_EN_VOL_SR 0x50 //Write enable volatile status register
#define WRITE_DISABLE 0x04
#define READ_STATUS_REG1 0x05
#define READ_STATUS_REG2 0x35
#define READ_STATUS_REG3 0x15
#define WRITE_STATUS_REG1 0x01
#define WRITE_STATUS_REG2 0x31
#define WRITE_STATUS_REG3 0x11
#define READ_DATA 0x03
#define FAST_READ 0x0B
#define PAGE_PRGM 0x02
#define SECTOR_ER_4KB 0x20
#define BLOCK_ER_32KB 0x52
#define BLOCK_ER_64KB 0xD8
#define CHIP_ER 0xC7
#define SUSPEND 0x75
#define RESUME 0x7A
#define POWER_DOWN_FLASH 0xB9
#define RELEASE_POWER_DOWN 0xAB
#define READ_MANUFACTURER_DEVICE_ID 0x90
#define READ_FLASH_UNIQUE_ID 0x4B
#define READ_SFDP_REG 0x5A
#define ERASE_SECURITY_REG 0x44
#define PRGM_SECURITY_REG 0x42
#define RD_SECURITY_REG 0x48
#define BLOCK_OR_SECTOR_LOCK 0x36
#define BLOCK_OR_SECTOR_UNLOCK 0x39
#define READ_BLCK_OR_SECTOR_LOCK 0x3D
#define GLOBAL_BLOCK_OR_SECTOR_LOCK 0x7E
#define GLOBAL_BLOCK_OR_SECTOR_UNLOCK 0x98
#define EN_RST 0x66
#define RESET 0x99

#define CS_LOW 0
#define CS_HIGH 1
#define FLASH_MANUFACTURER_ID 0xEF
#define FLASH_DEVICE_ID 0x14


#define RECENT_DEVICE_ON_TIME_ADDRESS 0X1FA000
#define CORRUPTED_FLASH_BLOCKS_ADDRESS 0x1F9000
#define NEXT_WRITE_ADDRESS 0x1F8000
#define NEXT_READ_ADDRESS 0x1F7000
#define NUMBER_OF_DATASETS_WRITTEN_ADDRESS 0x1F6000
#define NEXT_DATA_TO_BE_SENT_WRITE 0x1F5000
#define NUMBER_OF_CAR_DATASETS_WRITTEN_ADDRESS 0X1F4000
#define CAR_NEXT_READ_ADDRESS 0X1F3000
#define CAR_NEXT_WRITE_ADDRESS 0x1F2000
#define MAX_USABLE_FLASH_ADDRESS 0x1F0000 //1F0000


#define LAST_4KB_ADDRESS 0x1FF000

#define TIME_INFO_LENGTH 148
#define TIME_DATA 0
#define X_DATA 1
#define Y_DATA 2
#define Z_DATA 3
#define CLOSE 4
#define END_STRING 5



#define STATUS_REG1_DATA 0x00
#define STATUS_REG2_DATA 0x00
#define STATUS_REG3_DATA 0x04

#define NO_OF_ADRESSES_OF_LAST4KB_DATA 300

//Delays
#define DELAY_ERASE_32KB 400
#define DELAY_ERASE_64KB 1600
#define DELAY_CHIP_ERASE 25000
#define DELAY_SECTOR_ERASE_4KB 3
#define DELAY_RELEASE 1
#define DELAY_RST 1
#define DELAY_SUS 1
#define DELAY_TDP 1
#define DELAY_PAGE_PRGM 3
#define DELAY_WRITE_STATUS_REG 15


//Function declarations
void write_enable(struct io_descriptor *in_out);
void write_disable(struct io_descriptor *in_out);
void write_enable_volatile_reg(struct io_descriptor *in_out);
bool read_status_reg1(struct io_descriptor *in_out, uint8_t *regValue);
uint8_t read_status_reg2(struct io_descriptor *in_out);
uint8_t read_status_reg3(struct io_descriptor *in_out);
bool check_WEL_bit(struct io_descriptor *in_out);
bool check_BUSY_bit(struct io_descriptor *in_out);
bool check_SUS_bit(struct io_descriptor *in_out);
bool write_status_reg1(struct io_descriptor *in_out, uint8_t reg_data);
bool write_status_reg2(struct io_descriptor *in_out, uint8_t reg_data);
bool write_status_reg3(struct io_descriptor *in_out, uint8_t reg_data);
uint32_t read_data(struct io_descriptor *in_out, uint32_t address_data, uint32_t Number_of_data, uint8_t *read_data_array);
void fast_read(struct io_descriptor *in_out, uint32_t address_data, uint8_t Number_of_data, uint8_t read_data_array[]);
bool read_manufacturer_device_ID(struct io_descriptor *in_out, uint8_t array[]);
bool readFlashUniqueID(struct io_descriptor *in_out, uint8_t array[]);
uint32_t page_program(struct io_descriptor *in_out, uint32_t address_data, uint16_t Number_of_data, uint8_t write_data_array[]);
bool sector_erase4kb(struct io_descriptor *in_out, uint32_t address_data);
bool block_erase32kb(struct io_descriptor *in_out, uint32_t address_data);
bool block_erase64kb(struct io_descriptor *in_out, uint32_t address_data);
bool chip_erase(struct io_descriptor *in_out);
bool suspend(struct io_descriptor *in_out);
bool resume(struct io_descriptor *in_out);
bool pwr_dwn(struct io_descriptor *in_out);
bool pwr_dwn_release(struct io_descriptor *in_out);
uint32_t read_SFDP_reg(struct io_descriptor *in_out, uint8_t read_data_array[]);
//void erase_security_reg(struct io_descriptor *in_out);
//void read_security_reg(struct io_descriptor *in_out);
//void prgm_security_reg(struct io_descriptor *in_out);
bool block_sector_lock(struct io_descriptor *in_out, uint32_t address_data);
bool block_sector_unlock(struct io_descriptor *in_out, uint32_t address_data);
bool read_block_sec_lock(struct io_descriptor *in_out, uint32_t address_data);
bool global_block_sec_lock(struct io_descriptor *in_out);
bool global_block_sec_unlock(struct io_descriptor *in_out);
//void enable_rst(struct io_descriptor *in_out);
bool reset(struct io_descriptor *in_out);
//uint32_t writeIntoFlash(struct io_descriptor *in_out, uint8_t AG_data[],uint32_t Number_of_bytes);

uint32_t block32KB_address(uint8_t i);
uint32_t block64KB_address(uint8_t i);
bool write_ID_settings_data(struct io_descriptor *in_out, uint32_t write_address, uint8_t data[], uint16_t lengthOfData);
uint32_t one_KB_address(uint8_t i);
uint32_t sector4KB_address(uint8_t i);
uint32_t write_data(struct io_descriptor *in_out, uint32_t address_data, uint32_t number_of_bytes, uint8_t data_array[]);

uint32_t move_data(struct io_descriptor *in_out);
//uint32_t  write_next_address_data(struct io_descriptor *in_out, uint8_t Adata[]);

void update_pointers(struct io_descriptor *in_out);
//uint32_t  write_next_read_address(struct io_descriptor *in_out, uint8_t Adata[]);
bool isSpaceAvailableInFlash(struct io_descriptor *in_out);
bool isFlashReady(struct io_descriptor *in_out);

void makeFlashReadyForWrite(struct io_descriptor *ioFlash);
void unlockSectorsInFirstAndLastBlock(struct io_descriptor *in_out);
bool flashSelfTest(struct io_descriptor *in_out);
void updateFLASHcorruptedBlocks(struct io_descriptor *in_out, uint8_t data[]);
//bool testLastBlockOfFLASH(struct io_descriptor *in_out);

uint32_t writeIntoFlash(struct io_descriptor *in_out,uint32_t write_Address,uint32_t NoOfBytes,uint8_t data[]);
void storeNoOfDataSetsInFlash(struct io_descriptor *in_out,uint32_t write_Address,uint16_t datasets);
uint32_t readFromFlash(struct io_descriptor *in_out,uint32_t read_Address,uint32_t NoOfBytes,uint8_t *data);
uint32_t NoOfCARDataSetsStoredInFlash( struct io_descriptor *in_out);
uint32_t readNextWriteorReadAddress(struct io_descriptor *in_out,uint32_t write_Address);
uint16_t NoOfDataSetsRedFromFlash(struct io_descriptor *in_out, uint32_t dataSetsReadAddress);
bool writeNextAdrressIntoFlash(struct io_descriptor *in_out,uint32_t Address,uint32_t locationOfAddress);
#endif /* SPI_FLASH_H_ */
