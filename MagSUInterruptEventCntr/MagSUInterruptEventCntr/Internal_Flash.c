/*
* Internal_Flash.c
*
* Created: 24/8/2018 
*  Author: Jhansi
*/
#include <atmel_start.h>
#include "Internal_Flash.h"
#include <stdbool.h>

bool g_hwDebugMode;

/* */
bool writeSSIDIntoInternalFlash(uint8_t* ssidData, uint8_t ssidLength)
{
	bool result = false;
	uint8_t flashAccess = 1;
	if (ssidLength > M2M_MAX_SSID_LEN)
	{
		ssidLength = M2M_MAX_SSID_LEN;
	}
	flashAccess = flash_write(&FLASH_0,SSID_ADDRESS,(uint8_t *)ssidData,ssidLength) ;
	if(flashAccess == FLASH_ACCESS_SUCCESS)
	result = true;
	
	return result;
}

bool writePWDIntoInternalFlash(uint8_t* pwdData, uint8_t pwdLength)
{
	bool result = false;
	uint8_t flashAccess = 1;
	if (pwdLength > M2M_MAX_PSK_LEN)
	{
		pwdLength = M2M_MAX_PSK_LEN;
	}
	flashAccess = flash_write(&FLASH_0,PASSWORD_ADDRESS,(uint8_t *)pwdData,pwdLength);
	if(flashAccess == FLASH_ACCESS_SUCCESS)
	result = true;
	
	return result;
}


bool writeGWIPIntoInternalFLASH(uint32_t gatewaysIP)
{
	bool result = false;
	uint8_t gwIP[10] = {0};
	gwIP[0] = (uint8_t)(0x0000000F & gatewaysIP);
	gwIP[1] = (uint8_t)((0x000000F0 & gatewaysIP)>>4);
	gwIP[2] = (uint8_t)((0x00000F00 & gatewaysIP)>>8);
	gwIP[3] = (uint8_t)((0x0000F000 & gatewaysIP)>>12);
	gwIP[4] = (uint8_t)((0x000F0000 & gatewaysIP)>>16);
	gwIP[5] = (uint8_t)((0x00F00000 & gatewaysIP)>>20);
	gwIP[6] = (uint8_t)((0x0F000000 & gatewaysIP)>>24);
	gwIP[7] = (uint8_t)((0xF0000000 & gatewaysIP)>>28);
	
	if(g_hwDebugMode)
	{
		printf("GWIP in flash:");
		for(uint8_t ctr=0; ctr<8; ctr++)
		printf("%d.", gwIP[ctr]);	
		printf("\n");	
	}
	uint8_t flashAccess = 1;
	flashAccess = flash_write(&FLASH_0,GWIP_ADDRESS,&gwIP[0],8);
	if(flashAccess == FLASH_ACCESS_SUCCESS)
	result = true;
	if(g_hwDebugMode)
	printf("result of writeGWIPIntoFLASH:%d\n", result);
	return result;
}


bool writeTimeoutsIntoInternalFLASH(uint16_t APtimeout, uint16_t GWtimeout)
{
	bool result = false;
	uint8_t timeoutsArray[5] = {0};	
	timeoutsArray[0] = APtimeout & 0x00ff;
	timeoutsArray[1] = (uint8_t)((APtimeout & 0xff00) >> 8);
	
	timeoutsArray[2] = GWtimeout & 0x00ff;
	timeoutsArray[3] = (uint8_t)((GWtimeout & 0xff00) >> 8);
	
	/*
	if(g_hwDebugMode)
	{
		printf("Timeouts:");
		for(uint8_t ctr=0; ctr<8; ctr++)
		printf("%d.", gwIP[ctr]);
		printf("\n");
	}
	*/
	uint8_t flashAccess = 1;
	flashAccess = flash_write(&FLASH_0,TIMEOUT_ADDRESS,&timeoutsArray[0],4);
	if(flashAccess == FLASH_ACCESS_SUCCESS)
	{
		result = true;
		if(g_hwDebugMode)
		{
			printf("time out values were successfully written into FLASH\n");
		}
	}
	
	return result;
}

bool readGWIPFromInternalFLASH(void)
{
	bool result = false;
	uint8_t gatewayIP[9] = {0};
	uint8_t flashAccess = 1;
	flashAccess = flash_read(&FLASH_0,GWIP_ADDRESS, &gatewayIP[0],8);
	if(flashAccess == FLASH_ACCESS_SUCCESS)
		result = true;
			
	return result;
	
}
 
bool readtimeoutsFromInternalFLASH(uint16_t *APtimeOut, uint16_t *GWtimeOut)
{
	bool result = false;
	uint8_t readArray[5] = {0};
	uint8_t flashAccess = 1;
	flashAccess =  flash_read(&FLASH_0,TIMEOUT_ADDRESS,&readArray[0],4) ;
	if(flashAccess == FLASH_ACCESS_SUCCESS )
	result = true;
	
	*APtimeOut = ( ((uint16_t)(readArray[0])) | ( ( (uint16_t)(readArray[1])<<8) & 0xff00) );
	*GWtimeOut = ( ((uint16_t)(readArray[2])) | ( ( (uint16_t)(readArray[3])<<8) & 0xff00) );

	return result;
	
}

/*
* To read the SSID, Password and configuration information from flash
* @return, If any one of the data is missing it will return false, otherwise true.
* @arg ssidData - brings SSID information
* @arg pwdData - brings Password information
* @arg configData - brings configuration information
*/
bool ssidPwdAvailable(uint8_t *ssidData, uint8_t* pwdData)
{
	bool result = false;
	uint8_t loopCtr=0;
	uint8_t SSIDLength = 0;
	uint8_t PWDLength = 0;

	uint8_t ssidDataTemp[65] = {0}, pwdDataTemp[65] = {0};
	if(flash_read(&FLASH_0, SSID_ADDRESS,&ssidDataTemp[0],M2M_MAX_SSID_LEN ) == FLASH_ACCESS_SUCCESS)	// Read SSID from flash
	{
		// While storing in flash, SSID should be stored with NULL character
		for(loopCtr=0; (loopCtr<M2M_MAX_SSID_LEN && ssidDataTemp[loopCtr] != 255); loopCtr++);		// Calculates the length of SSID
		SSIDLength = loopCtr;
	
		if (g_hwDebugMode)
		{
			printf("SSID length in ssidPwdAvailable:%d\n", SSIDLength);
			printf("SSID: %s\n", ssidDataTemp);
		}
		
		if (checkSSID(ssidDataTemp, SSIDLength))											// Check if SSID is valid
		{
			if(flash_read(&FLASH_0,PASSWORD_ADDRESS,&pwdDataTemp[0], M2M_MAX_PSK_LEN)  == FLASH_ACCESS_SUCCESS)	// Read Password from flash
			{
				for(loopCtr=0;(loopCtr<M2M_MAX_PSK_LEN && pwdDataTemp[loopCtr]!=255); loopCtr++);		// Calculate the length of Password
				PWDLength = loopCtr;
			
				if (g_hwDebugMode)
				{
					printf("Password length in ssidPwdAvailable:%d\n", PWDLength);
					printf("PWD: %s\n", pwdDataTemp);
				}
			
				if(checkPWD(pwdDataTemp, PWDLength))									// Check if the password is valid
				{
				//	readConfig(in_out, configData, CONFIG_DATA_LENGTH);					// Read configuration data from flash
					
				//	if(checkIfConfigDataValid(configData))								// Check if the configuration data are valid
					{
						for (uint8_t i=0; i<SSIDLength; i++)
							ssidData[i] = ssidDataTemp[i];		// Copy the SSID data, only if all SSID, PWD, config are valid
							// as the address of the config structure variable assigned for SSID is received as the argument 
						for (uint8_t i=0;i<PWDLength; i++)
							pwdData[i] = pwdDataTemp[i];		// Copy the PWD data, only if all SSID, PWD, config are valid
							// as the address of the config structure variable assigned for PWD is received as the argument
						result = true;
						if (g_hwDebugMode)
						printf("SSID PWD - VALID\n");
					}
				}
			}
			else
			{
				if (g_hwDebugMode)
				printf("failed to read pwd\n");
			}
		}
		else
		{
			if (g_hwDebugMode)
			printf("checkSSID is false\n");
		}
	}
	else
	{
		if (g_hwDebugMode)
		printf("failed in read_data in ssid pwd available\n");
	}
	
	return result;
}


bool GWIPValid(uint32_t *gwIP)
{
	bool result = true;
	uint8_t ctr=0;
	uint8_t gateWayIP[9] = {0};
	
	if(flash_read(&FLASH_0, GWIP_ADDRESS,&gateWayIP[0],8) == FLASH_ACCESS_SUCCESS)
	{		
		for(ctr=0;(ctr<8 && result); ctr++)
		{
			/*if (g_hwDebugMode)
			printf("%d\n", gateWayIP[ctr]);*/
			if(gateWayIP[ctr] > 0x0F)
			result = false;
		}
		if(ctr == 8)
		{
			result = true;
			*gwIP = ( ((uint32_t)(gateWayIP[0])) | ((uint32_t)(gateWayIP[1])<<4) | ((uint32_t)(gateWayIP[2])<<8) | ((uint32_t)(gateWayIP[3])<<12) | \
			((uint32_t)(gateWayIP[4])<<16) | ((uint32_t)(gateWayIP[5])<<20) | ((uint32_t)(gateWayIP[6])<<24) | ((uint32_t)(gateWayIP[7])<<28) );
			if(g_hwDebugMode)
			{
			//	printf(" gatewaysIP:%ld\n", *gwIP);
				uint8_t *gateway1 = (uint8_t *)gwIP;
				printf("GW in readGWIPFromFLASH %u.%u.%u.%u\r\n",gateway1[0],gateway1[1],gateway1[2],gateway1[3]);
			}
		}		
	}
	else
		result = false;
	
	if (g_hwDebugMode)
	printf("GWIP is Valid\n\n");
	return result;
}


/** To check if the SSID is of length between 3 and 15 without NULL character
* Also, the characters should be having ASCII value between 0x21 and 0x7E		
* @return Returns true if the SSID has valid length and valid characters, Returns false otherwise
*/

bool checkSSID(uint8_t* ssidData, uint8_t ssidLength)
{
	bool result = true;

	if((ssidLength >= MIN_SSID_LENGTH)&&(ssidLength <= MAX_SSID_LENGTH))
	{
		for(uint8_t loopCtr=0; ((loopCtr<ssidLength) && (result == true)); loopCtr++)
		{
			// Accept any of the symbol whose ASCII value is between 0x21 and 0x7E and it can be NULL (End of string)
			if( !((ssidData[loopCtr] >= 0x21 && ssidData[loopCtr] <= 0x7E) || (ssidData[loopCtr] == 0))  ) //TODO EDITED 
			{
				result = false;		// If the character is invalid
			}
		}
	}
	else
		result = false;				// If the length is invalid

	return result;
}

/** To check if the PWD is of length between 8 and 15 without NULL character
* Also, the characters should be having ASCII value between 0x21 and 0x7E
* @return Returns true if the PWD has valid length and valid characters, Returns false otherwise
*/

bool checkPWD(uint8_t* pwdData, uint8_t pwdLength)
{
	bool result = true;
	if( (pwdLength>=MIN_PWD_LENGTH) && (pwdLength<=MAX_PWD_LENGTH) )
	{
		for(uint8_t loopCtr=0; ((loopCtr<pwdLength) && (result == true)); loopCtr++)
		{
			// Accept any of the symbol whose ASCII value is between 0x21 and 0x7E and it can be NULL (End of string)
			if( !((pwdData[loopCtr] >= 0x21 && pwdData[loopCtr] <= 0x7E) || (pwdData[loopCtr] == 0)) )
			{
				result = false;				// If the character is invalid
			}
		}
	}
	else
		result = false;						// If the length is invalid

	return result;
	
}

