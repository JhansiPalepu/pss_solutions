/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */
#ifndef ATMEL_START_PINS_H_INCLUDED
#define ATMEL_START_PINS_H_INCLUDED

#include <hal_gpio.h>

// SAMD21 has 8 pin functions

#define GPIO_PIN_FUNCTION_A 0
#define GPIO_PIN_FUNCTION_B 1
#define GPIO_PIN_FUNCTION_C 2
#define GPIO_PIN_FUNCTION_D 3
#define GPIO_PIN_FUNCTION_E 4
#define GPIO_PIN_FUNCTION_F 5
#define GPIO_PIN_FUNCTION_G 6
#define GPIO_PIN_FUNCTION_H 7

#define SWITCH GPIO(GPIO_PORTA, 2)
#define SPR1 GPIO(GPIO_PORTA, 4)
#define SENSOR_REG_EN GPIO(GPIO_PORTA, 6)
#define FLASH_CS GPIO(GPIO_PORTA, 7)
#define DBG GPIO(GPIO_PORTA, 10)
#define PWRONIND GPIO(GPIO_PORTA, 11)
#define PA12 GPIO(GPIO_PORTA, 12)
#define PA13 GPIO(GPIO_PORTA, 13)
#define CS_PIN GPIO(GPIO_PORTA, 14)
#define PA15 GPIO(GPIO_PORTA, 15)
#define SLAVE_SELECT GPIO(GPIO_PORTA, 17)
#define FLASH_MISO GPIO(GPIO_PORTA, 21)
#define FLASH_MOSI GPIO(GPIO_PORTA, 22)
#define FLASH_SCK GPIO(GPIO_PORTA, 23)
#define RESET_PIN GPIO(GPIO_PORTA, 27)
#define CE_PIN GPIO(GPIO_PORTA, 28)
#define CONF_WINC_EXT_INT_PIN GPIO(GPIO_PORTB, 9)
#define TXD GPIO(GPIO_PORTB, 10)
#define RXD GPIO(GPIO_PORTB, 11)
#define MEASEN GPIO(GPIO_PORTB, 22)

#endif // ATMEL_START_PINS_H_INCLUDED
